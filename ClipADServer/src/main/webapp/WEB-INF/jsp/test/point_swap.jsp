<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Clip - 포인트 스왑 테스트</title>

<script type="text/javascript" src="../js/common/jquery-1.8.3.js"></script>
<script type="text/javascript" src="../js/common/common.js"></script>
<script type="text/javascript" src="../js/common/jquery.json-2.4.js"></script>
<link rel="stylesheet" type="text/css" href="../css/mypoint.css"/>
<script type="text/javascript">

$(document).ready(function(){
	$("#btn_search").click(function(){
		goSearch();
	});
	
	$("#btn_swap").click(function(){
		goSwap();
	});
});

var goSearch = function(){

	var paramData = {
			"user_ci" : $("#user_ci").value
	};	
	
    $.ajax({
    	url: "/clip/pointcollect/getCardPoints.do",
        type: "POST",
        data: JSON.stringify(paramData),
        dataType: "json",
        contentType: 'application/json; charset=UTF-8',
        success: function (resData) {
        	
        	console.log("---------------------");
            console.log(resData);
            console.log("---------------------");
            poInfo = resData.pointInfo;
            
            var _hana = poInfo.hanaPoint;
            var _kb = poInfo.kbPoint;
            var _shinhan = poInfo.shinhanPoint;
            
            if(_hana.result == 'S') {
            	$('input[name=cardpoint]').eq(0).val(_hana.point);
            }
			if(_kb.result == 'S') {
				$('input[name=cardpoint]').eq(1).val(_kb.point);     	
			}
			if(_shinhan.result == 'S') {
				$('input[name=cardpoint]').eq(2).val(_shinhan.point);
			}
          
        },
        beforeSend: function () {
			alert(this.data);
        },
        error: function (e) {
            console.log("error:" + e.responseText);
            alert("네트워크 오류가 발생하였습니다.");
        }
    });
}


var goSwap = function(){

	var paramData = {
			"user_ci" : $("#user_ci").val(),
			"pointList" : []
		};	
	var _target = [];
	
	for (var i =0; i < 3; i++) {
		var _cm = $('input[name=compName]').eq(i).val();
		var _cp = $('input[name=cardpoint]').eq(i).val();
		var _po = $('input[name=point]').eq(i).val();
		//alert(_cm +" + "+_cp+" + "+_po );
		
		if(parseInt(_po) > 0){
			_target.push(_cm +"|"+_po);
		}
	}
	
	paramData.pointList = _target;
	console.log(paramData);
	
    $.ajax({
    	url: "/clip/pointcollect/swapCardPoints.do",
        type: "POST",
        data: JSON.stringify(paramData),
        dataType: "json",
        contentType: 'application/json; charset=UTF-8',
        success: function (resData) {
            console.log(resData);
        },
        beforeSend: function () {
			alert(this.data);
        },
        error: function (e) {
            console.log("error:" + e.responseText);
            alert("네트워크 오류가 발생하였습니다.");
        }
    });
}

</script>

</head>
<body>

<form name="submitForm" id="submitForm">

<div style="margin:100px;">

<h1>포인트 SWAP 테스트</h1>
<p/>
user_ci : <input type="text" name="user_ci" id="user_ci" value="" width="30"/>
<p/>
<table style="border-style:solid;border:1px;">
  <tr>
	<td width="100px;"> 카드사 </td><td> 현재 포인트 </td><td> 차감 포인트 </td>
  </tr>
  <tr>
	<td>하나멤버스<input type="hidden" name="compName" id="compName0" value="hanamembers"/></td>
	<td><input type="text" name="cardpoint" id="cardpoint0" value=""/></td>
	<td><input type="text" name="point" id="point0" value=""/></td>
  </tr>
  <tr>
	<td>KB<input type="hidden" name="compName" id="compName1" value="kb"/></td>
	<td><input type="text" name="cardpoint" id="cardpoint1" value=""/></td>
	<td><input type="text" name="point" id="point1" value=""/></td>
  </tr>
  <tr>
	<td>신한<input type="hidden" name="compName" id="compName2" value="shinhan"/></td>
	<td><input type="text" name="cardpoint" id="cardpoint2" value=""/></td>
	<td><input type="text" name="point" id="point2" value=""/></td>
  </tr>
</table>
<p/>
 <input type="button" name="btn_search" id="btn_search" value=" 조회 "> &nbsp; &nbsp; <input type="button" name="btn_swap" id="btn_swap" value=" 스왑 ">
</div>
</form>
</body>
</html>