package clip.pointswap.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.coopnc.cardpoint.CardPointSwapManager;
import com.coopnc.cardpoint.consts.Consts.TrType;
import com.coopnc.cardpoint.domain.PointSwapInfo;
import com.coopnc.cardpoint.handler.PointHandler_HANA;
import com.coopnc.cardpoint.util.MessageUtil;

import clip.clippoint.service.ClipPointService;
import clip.common.crypto.AES256CipherClip;
import clip.common.util.DateUtil;
import clip.integration.bean.BcCardMessage;
import clip.integration.bean.HanaCardMessage;
import clip.integration.bean.KbCardMessage;
import clip.integration.bean.PointSwapCancelVo;
import clip.integration.bean.ShinhanCardMessage;
import clip.integration.service.BcCardClientService;
import clip.integration.service.HanaCardClientService;
import clip.integration.service.KbCardClientService;
import clip.integration.service.ShinhanCardClientService;
import clip.pointswap.dao.PointSwapDao;
import clip.roulette.bean.CardPointInfo;
import clip.roulette.bean.PointSwap;

@Service("pointSwapService")
public class PointSwapServiceImpl implements PointSwapService {
	
	Logger logger = Logger.getLogger(this.getClass());
	
	@Value("#{props['POINT_SWAP_INI_PATH']}") private String pointSwapIniPath;
	
	@Value("#{props['POINT_SWAP_MAX_HANA']}") private int POINT_SWAP_MAX_HANA;
	@Value("#{props['POINT_SWAP_MAX_KB']}") private int POINT_SWAP_MAX_KB;
	@Value("#{props['POINT_SWAP_MAX_SHINHAN']}") private int POINT_SWAP_MAX_SHINHAN;
	
	@Value("#{props['POINT_SWAP_MAX_TOTAL']}") private int POINT_SWAP_MAX_TOTAL;
	@Value("#{props['POINT_SWAP_DAY_MAX_HANA']}") private int POINT_SWAP_DAY_MAX_HANA;
	
	
	@Autowired
	private PointSwapDao pointSwapDao;

	@Autowired
	private ClipPointService clipPointService;
	
	@Autowired
	private HanaCardClientService hanaCardClientService;
	
	@Autowired
	private KbCardClientService kbCardClientService;
	
	@Autowired
	private ShinhanCardClientService shinhanCardClientService;
	
	@Autowired
	private BcCardClientService bcCardClientService;
	
	
	@Override
	public void testSwap() throws Exception {
		CardPointSwapManager manager = new CardPointSwapManager(pointSwapIniPath);
		
		PointSwapInfo pointSwapInfo = new PointSwapInfo();
		pointSwapInfo.setUser_ci("12345");
		//pointSwapInfo.setCard_point(299);
		pointSwapInfo.setTrans_id(MessageUtil.lpad(""+pointSwapDao.selectDailySerialNo("hanamembers"), 10, "0"));
		pointSwapInfo.setTrans_div(TrType.GET.getValue());
		pointSwapInfo.setTrans_req_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
			
		manager.setHandler(new PointHandler_HANA(pointSwapInfo)).getPoint();
	}

	@Override
	public PointSwap getCardPoints(HttpServletRequest request, PointSwap param) throws Exception {
		PointSwap result = new PointSwap();
		
		// Start the clock
        long start = System.currentTimeMillis();
		logger.info("PointSwap Get CALL : " + MessageUtil.getDateStr("hhmmss SSS") +" ============");
		
		HanaCardMessage hanaMsg = new HanaCardMessage();
		hanaMsg.setUser_ci(param.getUser_ci());
		Future<HanaCardMessage> hanaResult = hanaCardClientService.getPoint(hanaMsg);
		//cardpointswap.jar 를 통해 카드사와 통신 
		
		KbCardMessage kbMsg = new KbCardMessage();
		kbMsg.setUser_ci(param.getUser_ci());
		Future<KbCardMessage> kbResult = kbCardClientService.getPoint(kbMsg);
		
		
		ShinhanCardMessage shinhanMsg = new ShinhanCardMessage();
		shinhanMsg.setUser_ci(param.getUser_ci());
		shinhanMsg.setCust_id_org(AES256CipherClip.AES_Decode(param.getCust_id()));
		Future<ShinhanCardMessage> shinhanResult = shinhanCardClientService.getPoint(shinhanMsg);
		
		
		BcCardMessage bcMsg = new BcCardMessage();
		bcMsg.setUser_ci(param.getUser_ci());
		Future<BcCardMessage> bcResult = bcCardClientService.getPoint(bcMsg);
		
		logger.info("PointSwap Get WAIT : " + MessageUtil.getDateStr("hhmmss SSS") +" ============");
		
		HanaCardMessage hanaPoint = hanaResult.get();
		KbCardMessage kbPoint = kbResult.get();
		ShinhanCardMessage shinhanPoint = shinhanResult.get();
		BcCardMessage bcPoint = bcResult.get();
		
		logger.info("PointSwap Get Response hana result["+hanaPoint.getResult()+"],resultCode["+hanaPoint.getRes_code()+"],point["+hanaPoint.getAvail_point()+"]");
		logger.info("PointSwap Get Response kb result["+kbPoint.getResult()+"],resultCode["+kbPoint.getRes_code()+"],point["+kbPoint.getAvail_point()+"]");
		logger.info("PointSwap Get Response shinhan result["+shinhanPoint.getResult()+"],resultCode["+shinhanPoint.getRes_code()+"],point["+shinhanPoint.getAvail_point()+"]");
		logger.info("PointSwap Get Response bc result["+bcPoint.getResult()+"],resultCode["+bcPoint.getRes_code()+"],point["+bcPoint.getAvail_point()+"]");
		logger.debug("PointSwap Get COMPLETE : " + MessageUtil.getDateStr("hhmmss SSS") +" ============");
		
		result.setHanaPoint(new CardPointInfo());
		result.getHanaPoint().setCardName("hanamembers");
		result.getHanaPoint().setResult(hanaPoint.getResult());
		if("S".equals(hanaPoint.getResult())) {
			result.getHanaPoint().setPoint(""+hanaPoint.getAvail_point());
		}
		
		//TODO KB 적용 대기중
//		result.setKbPoint(new CardPointInfo());
//		result.getKbPoint().setCardName("kbpointree");
//		result.getKbPoint().setResult("F");
		result.setKbPoint(new CardPointInfo());
		result.getKbPoint().setCardName("kbpointree");
		result.getKbPoint().setResult(kbPoint.getResult());
		if("S".equals(kbPoint.getResult())) {
			result.getKbPoint().setPoint(""+kbPoint.getAvail_point());
		}
		
		result.setShinhanPoint(new CardPointInfo());
		result.getShinhanPoint().setCardName("shinhan");
		result.getShinhanPoint().setResult(shinhanPoint.getResult());
		if("S".equals(shinhanPoint.getResult())) {
			result.getShinhanPoint().setPoint(""+shinhanPoint.getAvail_point());
		}
		
		//TODO BC 적용 대기중
		result.setBcPoint(new CardPointInfo());
		result.getBcPoint().setCardName("bccard");
		result.getBcPoint().setResult(bcPoint.getResult());
		if("S".equals(bcPoint.getResult())) {
			result.getBcPoint().setPoint(""+bcPoint.getAvail_point());
		}
		
        // Print results, including elapsed time
        logger.info("PointSwap Get card point total Elapsed time : " + (System.currentTimeMillis() - start));

		result.setUser_ci(param.getUser_ci());

		return result;
	}

	@Override
	public PointSwap swapCardPoints(HttpServletRequest request, PointSwap param) throws Exception {
		PointSwap result = new PointSwap();
		
        long start = System.currentTimeMillis();
        logger.info("PointSwap Use CALL : " + MessageUtil.getDateStr("hhmmss SSS") +" ============");
        
		//----------------- 전환 포인트 제한 체크를 위해 오늘 전환 포인트 조회 ------
//		PointSwapInfo tester = new PointSwapInfo();
//		tester.setUser_ci(param.getUser_ci());
//		tester.setTrans_req_date(MessageUtil.getDateStr("yyyyMMdd"));
//		Map<String, Integer> maxPoints = pointSwapDao.selectTodaySwapPoint(tester);
        Map<String, Object> tester = new HashMap<String, Object>();
        tester.put("user_ci", param.getUser_ci());
        tester.put("trans_req_date", MessageUtil.getDateStr("yyyyMMdd"));
        tester.put("trans_req_month", MessageUtil.getDateStr("yyyyMM"));
        
        //clip쪽 총 제한 포인트 스왑 검사
		List<Map<String, Object>> pointInfo = pointSwapDao.selectTodaySwapPoint(tester);
		int maxDayHanaMoney = 0;
		int maxMonthHanaMoney = 0;
		int maxDayHanaCnt = 0;
		int maxDayKbMoney = 0;
		@SuppressWarnings("unused")
		int maxMonthKbMoney = 0;
		int maxDayShinhanMoney = 0;
		@SuppressWarnings("unused")
		int maxMonthShinhanMoney = 0;
		int maxDayBcMoney = 0;
		@SuppressWarnings("unused")
		int maxMonthBcMoney = 0;
		// [jaeheung] 2017.08.17 일일 전환 횟수 체크
		 
//		int maxHanaMoney = maxPoints.get("hanamembers") == null ? 0 : maxPoints.get("hanamembers");
//		int maxKbMoney = maxPoints.get("kbpointree") == null ? 0 : maxPoints.get("kbpointree");
//		int maxShinhanMoney = maxPoints.get("shinhan") == null ? 0 : maxPoints.get("shinhan");
		for(Map<String, Object> pointTemp : pointInfo) {
			String regSource = pointTemp.get("reg_source").toString();
			if(pointTemp.get("res_date_gb").toString().equals("M")) {//res_date_gb: D,M
				switch(regSource) {
					case "hanamembers" : 
						maxMonthHanaMoney = pointTemp.get("req_point").toString() == null ? 0 : Integer.parseInt(pointTemp.get("req_point").toString());
						//그 달에 전환한 포인트 변수
						break;
					case "kbpointree" : 
						maxMonthKbMoney = pointTemp.get("req_point").toString() == null ? 0 : Integer.parseInt(pointTemp.get("req_point").toString());
						break;
					case "shinhan" : 
						maxMonthShinhanMoney = pointTemp.get("req_point").toString() == null ? 0 : Integer.parseInt(pointTemp.get("req_point").toString());
						break;
					case "bccard" : 
						maxMonthBcMoney = pointTemp.get("req_point").toString() == null ? 0 : Integer.parseInt(pointTemp.get("req_point").toString());
						break;
					default : 
						break;
				}
			} else {
				switch(regSource) {
					case "hanamembers" : 
						maxMonthHanaMoney = pointTemp.get("req_point").toString() == null ? 0 : Integer.parseInt(pointTemp.get("req_point").toString());
						maxDayHanaCnt = pointTemp.get("res_day_count").toString() == null ? 0 : Integer.parseInt(pointTemp.get("res_day_count").toString());
						//하나는 하루 10회 제한
						break;
					case "kbpointree" : 
						maxMonthKbMoney = pointTemp.get("req_point").toString() == null ? 0 : Integer.parseInt(pointTemp.get("req_point").toString());
						break;
					case "shinhan" : 
						maxMonthShinhanMoney = pointTemp.get("req_point").toString() == null ? 0 : Integer.parseInt(pointTemp.get("req_point").toString());
						break;
					case "bccard" : 
						maxMonthBcMoney = pointTemp.get("req_point").toString() == null ? 0 : Integer.parseInt(pointTemp.get("req_point").toString());
						break;
					default : 
						break;
				}
			}
		}
		
        //----------------- 0일때는 전환 할 필요 없으므로 기준으로 삼기위해 미리 추출 ------
		int hanaMoney = parsePoint(param.getHanaPoint().getPoint());
        int kbMoney = parsePoint(param.getKbPoint().getPoint());
        int shinhanMoney = parsePoint(param.getShinhanPoint().getPoint());
        int bcMoney = parsePoint(param.getBcPoint().getPoint());
        
        logger.info("PointSwap Use request : hanaMoney = "+hanaMoney+", kbMoney = "+kbMoney+", shinhanMoney = "+shinhanMoney+", bcMoney = "+bcMoney);
        
        
        //----------------- 전송시 사용할 각각의 메시지 작성 ---------------------
		HanaCardMessage hanaMsg = new HanaCardMessage();
		hanaMsg.setUser_ci(param.getUser_ci());
		hanaMsg.setReq_point(hanaMoney);
		hanaMsg.setPointOver(maxMonthHanaMoney + hanaMoney > POINT_SWAP_MAX_HANA);//true일경우 에러
		
		if(maxDayHanaCnt >= POINT_SWAP_DAY_MAX_HANA) {
			hanaMsg.setCountOver(true);
		}
		
		KbCardMessage kbMsg = new KbCardMessage();
		kbMsg.setUser_ci(param.getUser_ci());
		kbMsg.setReq_point(kbMoney);
		kbMsg.setPointOver( maxMonthKbMoney + kbMoney > POINT_SWAP_MAX_KB);
		
		ShinhanCardMessage shinhanMsg = new ShinhanCardMessage();
		shinhanMsg.setUser_ci(param.getUser_ci());
		shinhanMsg.setCust_id_org(AES256CipherClip.AES_Decode(param.getCust_id()));
		shinhanMsg.setReq_point(shinhanMoney);
		shinhanMsg.setPointOver( maxDayShinhanMoney + shinhanMoney > POINT_SWAP_MAX_SHINHAN);
		
		BcCardMessage bcMsg = new BcCardMessage();
		bcMsg.setUser_ci(param.getUser_ci());
		bcMsg.setReq_point(bcMoney);
		
		// [jaeheung] 2017.08.11 일일 총한도 조회 위해 조회된 금일 카드사 전환 포인트 취합
		// TODO: bc는 총한도 없으므로 확인 필요.
		boolean isTotalPointOver = false;
		//maxDayHanaMoney: 금일 전환된 하나포인트, hanaMoney: 전환하려는 하나 포인트
		int maxTotal = (maxDayHanaMoney+hanaMoney) + (maxDayKbMoney+kbMoney) + (maxDayShinhanMoney+shinhanMoney) + (maxDayBcMoney+bcMoney);
  		if(POINT_SWAP_MAX_TOTAL <= maxTotal) {
  			hanaMsg.setPointOver(true);
  			kbMsg.setPointOver(true);
  			shinhanMsg.setPointOver(true);
  			bcMsg.setPointOver(true);
  			isTotalPointOver = true;
  		}
		
		
		//----------------- 비동기로 각각을 모두 호출 ---------------------
		Future<HanaCardMessage> hanaResult = hanaCardClientService.minusPoint(hanaMsg);
		Future<KbCardMessage> kbResult = kbCardClientService.minusPoint(kbMsg);
		Future<ShinhanCardMessage> shinhanResult = shinhanCardClientService.minusPoint(shinhanMsg);
		Future<BcCardMessage> bcResult = bcCardClientService.minusPoint(bcMsg);
		
		logger.debug("PointSwap Use WAIT : " + MessageUtil.getDateStr("hhmmss SSS") +" ============");

		//----------------- 결과를 취합 -----------------------------
		HanaCardMessage hanaPoint = hanaResult.get();
		KbCardMessage kbPoint = kbResult.get();
		ShinhanCardMessage shinhanPoint = shinhanResult.get();
		BcCardMessage bcPoint = bcResult.get();

		logger.info("PointSwap Use Response hana result["+hanaPoint.getResult()+"],resultCode["+hanaPoint.getRes_code()+"], avail point["+hanaPoint.getAvail_point() +"], use point["+hanaPoint.getRes_point()+"]");
		logger.info("PointSwap Use Response kb result["+kbPoint.getResult()+"],resultCode["+kbPoint.getRes_code()+"], avail point["+kbPoint.getAvail_point() +"], use point["+kbPoint.getRes_point()+"]");	
		logger.info("PointSwap Use Response shinhan result["+shinhanPoint.getResult()+"],resultCode["+shinhanPoint.getRes_code()+"], avail point["+shinhanPoint.getAvail_point() +"], use point["+shinhanPoint.getRes_point()+"]");
		logger.info("PointSwap Use Response bc result["+bcPoint.getResult()+"],resultCode["+bcPoint.getRes_code()+"], avail point["+bcPoint.getAvail_point() +"], use point["+bcPoint.getRes_point()+"]");
		
		logger.debug("PointSwap Use COMPLETE : " + MessageUtil.getDateStr("hhmmss SSS") +" ============");
		
		boolean totalCheck = false;
		

		//---------- 하나 멤버스 처리 결과 ------------------
		result.setHanaPoint(new CardPointInfo());
		result.getHanaPoint().setCardName("hanamembers");
		result.getHanaPoint().setResult(hanaPoint.getResult());
		result.getHanaPoint().setPointOver(hanaPoint.isPointOver());
		
		if(!hanaMsg.isPointOver() && hanaMsg.getReq_point() > 0 && !hanaMsg.isCountOver()) {
			if("S".equals(result.getHanaPoint().getResult())) {
				result.getHanaPoint().setPoint(""+hanaPoint.getRes_point());
				
				int ret = 0;	
				try {
					ret = clipPointService.procClipPoint(request, param, 0, 0, MessageUtil.getDateStr("yyyyMMdd")+hanaMsg.getTrans_id(), hanaMsg.getRes_point());		
				} catch (Exception e){
					e.printStackTrace();
					logger.error("하나 포인트 적립중 에러 발생", e);
					logger.info("PointSwap Use hanamembers Error [" + e.getMessage()+"]");
				}

				if(ret != 1) {
					PointSwapInfo cancelInfo = pointSwapDao.selectSwapListForTransId(hanaMsg);
					
					pointSwapDao.insertPointSwapCancelInfo(cancelInfo);
					logger.info("PointSwap Cancel Info Insert hanamembers old_swap_idx["+cancelInfo.getIdx()+"],reg_source["+cancelInfo.getReg_source()+"], reg_date["+cancelInfo.getTrans_req_date() +"]");
//					hanaCardClientService.cancelMinusPoint(hanaMsg);
					result.getHanaPoint().setResult("F");
					result.getHanaPoint().setResultMsg("포인트 전환에 실패하였습니다.");
					logger.info("PointSwap Use hanamembers Fail [" + result.getHanaPoint().getResultMsg()+"]");
				} else {
					totalCheck = true;
					logger.info("PointSwap Use hanamembers success");
				}
			} else {
				result.getHanaPoint().setResultMsg("포인트 전환에 실패하였습니다.");
				logger.info("PointSwap Use hanamembers Fail [" + result.getHanaPoint().getResultMsg()+"]");
			}
		}
		else if(hanaMsg.getReq_point() == 0) {
			// TODO
			result.getHanaPoint().setResult("N");
		}
		else if(hanaMsg.isCountOver()) {
			result.getHanaPoint().setResult("C");
			result.getHanaPoint().setResultMsg("일일 전환 횟수를 초과하였습니다.");
			logger.info("PointSwap Use hanamembers Fail [" + result.getHanaPoint().getResultMsg()+"]");
		}
		else {
			if(hanaMsg.isPointOver()) {
				result.getHanaPoint().setResult("F");
				result.getHanaPoint().setPointOver(true);
				result.getHanaPoint().setResultMsg("일일 전환 상한액을 초과하였습니다.");
				logger.info("PointSwap Use hanamembers Fail [" + result.getHanaPoint().getResultMsg()+"]");
			}
		}
		
		//---------- KB 처리 결과 ------------------
		result.setKbPoint(new CardPointInfo());
		result.getKbPoint().setCardName("kbpointree");
		result.getKbPoint().setResult(kbPoint.getResult());
		
		if(!kbMsg.isPointOver() && kbMsg.getReq_point() > 0) {
			if("S".equals(result.getKbPoint().getResult())) {
				result.getKbPoint().setPoint(""+kbPoint.getRes_point());
				
				int ret = 0;
				try {
					ret = clipPointService.procClipPoint(request, param, 1, 0, MessageUtil.getDateStr("yyyyMMdd")+kbMsg.getTrans_id(), kbMsg.getRes_point());
				} catch (Exception e){
					e.printStackTrace();
					logger.error("kb 포인트 적립중 에러 발생", e);
					logger.info("PointSwap Use kbpointree Error [" + e.getMessage()+"]");
				}

				if(ret != 1) {
//					kbCardClientService.cancelMinusPoint(kbMsg);
					PointSwapInfo cancelInfo = pointSwapDao.selectSwapListForTransId(kbMsg);
					
					pointSwapDao.insertPointSwapCancelInfo(cancelInfo);
					logger.info("PointSwap Cancel Info Insert kbpointree old_swap_idx["+cancelInfo.getIdx()+"],reg_source["+cancelInfo.getReg_source()+"], reg_date["+cancelInfo.getTrans_req_date() +"]");
					result.getKbPoint().setResult("F");
					result.getKbPoint().setResultMsg("포인트 전환에 실패하였습니다.");
					logger.info("PointSwap Use kbpointree Fail [" + result.getKbPoint().getResultMsg()+"]");
				} else {
					totalCheck = true;
					logger.info("PointSwap Use kbpointree success");
				}
				
				
			} else {
				result.getKbPoint().setResultMsg("포인트 전환에 실패하였습니다.");
				logger.info("PointSwap Use kbpointree Fail [" + result.getKbPoint().getResultMsg()+"]");
			}
		}
		else if(kbMsg.getReq_point() == 0) {
			// TODO
			result.getKbPoint().setResult("N");
		}
		else {
			if(kbMsg.isPointOver()) {
				result.getKbPoint().setResult("F");
				result.getKbPoint().setPointOver(true);
				result.getKbPoint().setResultMsg("일일 전환 상한액을 초과하였습니다.");
				logger.info("PointSwap Use kbpointree Fail [" + result.getKbPoint().getResultMsg()+"]");
			}
		}
		
		
		//---------- 신한  처리 결과 ------------------
		result.setShinhanPoint(new CardPointInfo());
		result.getShinhanPoint().setCardName("shinhan");
		result.getShinhanPoint().setResult(shinhanPoint.getResult());
		
		if(!shinhanMsg.isPointOver() && shinhanMsg.getReq_point() > 0) {
			if("S".equals(result.getShinhanPoint().getResult())) {
				result.getShinhanPoint().setPoint(""+shinhanPoint.getRes_point());
				
				int ret = 0;
				
				try {
					ret = clipPointService.procClipPoint(request, param, 2, 0, MessageUtil.getDateStr("yyyyMMdd")+shinhanMsg.getTrans_id(), shinhanMsg.getRes_point());
				} catch (Exception e){
					e.printStackTrace();
					logger.error("신한 포인트 적립중 에러 발생", e);
					logger.info("PointSwap Use Shinhan Error [" + e.getMessage()+"]");
				}

				if(ret != 1) {
//					shinhanCardClientService.cancelMinusPoint(shinhanMsg);
					PointSwapInfo cancelInfo = pointSwapDao.selectSwapListForTransId(shinhanMsg);
					
					pointSwapDao.insertPointSwapCancelInfo(cancelInfo);
					logger.info("PointSwap Cancel Info Insert Shinhan old_swap_idx["+cancelInfo.getIdx()+"],reg_source["+cancelInfo.getReg_source()+"], reg_date["+cancelInfo.getTrans_req_date() +"]");
					result.getShinhanPoint().setResult("F");
					result.getShinhanPoint().setResultMsg("포인트 전환에 실패하였습니다.");
					logger.info("PointSwap Use Shinhan Fail [" + result.getShinhanPoint().getResultMsg()+"]");
				} else {
					totalCheck = true;
					logger.info("PointSwap Use Shinhan pointswap success");
				}
				
			} else {
				result.getShinhanPoint().setResultMsg("포인트 전환에 실패하였습니다.");
				logger.info("PointSwap Use Shinhan Fail [" + result.getShinhanPoint().getResultMsg()+"]");
			}
		}
		else if(shinhanMsg.getReq_point() == 0) {
			// TODO
			result.getShinhanPoint().setResult("N");
		}
		else {
			if(shinhanMsg.isPointOver()){
				result.getShinhanPoint().setResult("F");
				result.getShinhanPoint().setPointOver(true);
				result.getShinhanPoint().setResultMsg("일일 전환 상한액을 초과하였습니다.");
				logger.info("PointSwap Use Shinhan Fail [" + result.getShinhanPoint().getResultMsg()+"]");
			}	
		}
		
		//---------- BC  카드  처리 결과 ------------------
		result.setBcPoint(new CardPointInfo());
		result.getBcPoint().setCardName("bccard");
		result.getBcPoint().setResult(bcPoint.getResult());
		
		if(!bcMsg.isPointOver() && bcMsg.getReq_point() > 0) {
			if("S".equals(result.getBcPoint().getResult())) {
				result.getBcPoint().setPoint(""+bcPoint.getRes_point());
				
				int ret = 0;
				
				try {
					ret = clipPointService.procClipPoint(request, param, 3, 0, MessageUtil.getDateStr("yyyyMMdd")+bcMsg.getTrans_id(), bcMsg.getRes_point());
				} catch (Exception e){
					e.printStackTrace();
					logger.error("비씨 포인트 적립중 에러 발생", e);
					logger.info("PointSwap Use BC Error [" + e.getMessage()+"]");
				}

				if(ret != 1) {
					PointSwapInfo cancelInfo = pointSwapDao.selectSwapListForTransId(bcMsg);
					
					pointSwapDao.insertPointSwapCancelInfo(cancelInfo);
					logger.info("PointSwap Cancel Info Insert BC old_swap_idx["+cancelInfo.getIdx()+"],reg_source["+cancelInfo.getReg_source()+"], reg_date["+cancelInfo.getTrans_req_date() +"]");
					result.getBcPoint().setResult("F");
					result.getBcPoint().setResultMsg("포인트 전환에 실패하였습니다.");
					logger.info("PointSwap Use BC Fail [" + result.getBcPoint().getResultMsg()+"]");
				} else {
					totalCheck = true;
					logger.info("PointSwap Use BC pointswap success");
				}
				
			} else {
				result.getBcPoint().setResultMsg("포인트 전환에 실패하였습니다.");
				logger.info("PointSwap Use BC Fail [" + result.getBcPoint().getResultMsg()+"]");
			}
		}
		else if(bcMsg.getReq_point() == 0) {
			// TODO
			result.getBcPoint().setResult("N");
		}
		else {
			if(bcMsg.isPointOver()){
				result.getBcPoint().setResult("F");
				result.getBcPoint().setPointOver(true);
				result.getBcPoint().setResultMsg("일일 전환 상한액을 초과하였습니다.");
				logger.info("PointSwap Use BC Fail [" + result.getBcPoint().getResultMsg()+"]");
			}	
		}
		
		
		if(!totalCheck) {
			result.setResult("F");
			//---------------------- [jaeheung] 2017.08.11 일일 총 상한액 초과시 분기 처리
			if(isTotalPointOver) {
				result.setResultMsg("일일 전환 총 상한포인트 " + POINT_SWAP_MAX_TOTAL + "을 초과하였습니다.");
				logger.info("PointSwap Use Fail [" + result.getResultMsg()+"]");
			} else {
				result.setResultMsg("포인트 전환에 실패하였습니다.");
				logger.info("PointSwap Use Fail [" + result.getResultMsg()+"]");
			}
		} else {
			result.setResult("S");
		}
		
        // Print results, including elapsed time
        logger.info("PointSwap Use Elapsed time: " + (System.currentTimeMillis() - start));

		result.setUser_ci(param.getUser_ci());

		return result;
	}

	@Override
	public PointSwap getCardPointsTest(HttpServletRequest request, PointSwap param) throws Exception {
		PointSwap result = new PointSwap();
		
		// Start the clock
        long start = System.currentTimeMillis();
		logger.info("PointSwap Get CALL : " + MessageUtil.getDateStr("hhmmss SSS") +" ============");
		
		HanaCardMessage hanaMsg = new HanaCardMessage();
		hanaMsg.setUser_ci(param.getUser_ci());
		Future<HanaCardMessage> hanaResult = hanaCardClientService.getPoint(hanaMsg);
		
		//TODO KB 적용 대기중
		KbCardMessage kbMsg = new KbCardMessage();
		kbMsg.setUser_ci(param.getUser_ci());
		Future<KbCardMessage> kbResult = kbCardClientService.getPoint(kbMsg);
		
		ShinhanCardMessage shinhanMsg = new ShinhanCardMessage();
		shinhanMsg.setUser_ci(param.getUser_ci());
		shinhanMsg.setCust_id_org(AES256CipherClip.AES_Decode(param.getCust_id()));
		Future<ShinhanCardMessage> shinhanResult = shinhanCardClientService.getPoint(shinhanMsg);
		
		//TODO BC 적용 대기중
		BcCardMessage bcMsg = new BcCardMessage();
		bcMsg.setUser_ci(param.getUser_ci());
		Future<BcCardMessage> bcResult = bcCardClientService.getPoint(bcMsg);
		
		logger.info("PointSwap Get WAIT : " + MessageUtil.getDateStr("hhmmss SSS") +" ============");
		
		HanaCardMessage hanaPoint = hanaResult.get();
		//TODO KB 적용 대기중
		KbCardMessage kbPoint = kbResult.get();
		ShinhanCardMessage shinhanPoint = shinhanResult.get();
		//TODO BC 적용 대기중
		BcCardMessage bcPoint = bcResult.get();
		
		logger.info("PointSwap Get Response hana result["+hanaPoint.getResult()+"],resultCode["+hanaPoint.getRes_code()+"],point["+hanaPoint.getAvail_point()+"]");
		//TODO KB 적용 대기중
		logger.info("PointSwap Get Response kb result["+kbPoint.getResult()+"],resultCode["+kbPoint.getRes_code()+"],point["+kbPoint.getAvail_point()+"]");
//		logger.info("PointSwap KB Sevice not ready");
		logger.info("PointSwap Get Response shinhan result["+shinhanPoint.getResult()+"],resultCode["+shinhanPoint.getRes_code()+"],point["+shinhanPoint.getAvail_point()+"]");
		//TODO BC 적용 대기중
		logger.info("PointSwap Get Response bc result["+bcPoint.getResult()+"],resultCode["+bcPoint.getRes_code()+"],point["+bcPoint.getAvail_point()+"]");

		logger.debug("PointSwap Get COMPLETE : " + MessageUtil.getDateStr("hhmmss SSS") +" ============");
		
		result.setHanaPoint(new CardPointInfo());
		result.getHanaPoint().setCardName("hanamembers");
		result.getHanaPoint().setResult(hanaPoint.getResult());
		if("S".equals(hanaPoint.getResult())) {
			result.getHanaPoint().setPoint(""+hanaPoint.getAvail_point());
		}
		
		//TODO KB 적용 대기중
//		result.setKbPoint(new CardPointInfo());
//		result.getKbPoint().setCardName("kbpointree");
//		result.getKbPoint().setResult("F");
		result.setKbPoint(new CardPointInfo());
		result.getKbPoint().setCardName("kbpointree");
		result.getKbPoint().setResult(kbPoint.getResult());
		if("S".equals(kbPoint.getResult())) {
			result.getKbPoint().setPoint(""+kbPoint.getAvail_point());
		}
		
		result.setShinhanPoint(new CardPointInfo());
		result.getShinhanPoint().setCardName("shinhan");
		result.getShinhanPoint().setResult(shinhanPoint.getResult());
		if("S".equals(shinhanPoint.getResult())) {
			result.getShinhanPoint().setPoint(""+shinhanPoint.getAvail_point());
		}
		
		//TODO BC 적용 대기중
		result.setBcPoint(new CardPointInfo());
		result.getBcPoint().setCardName("bccard");
		result.getBcPoint().setResult(bcPoint.getResult());
		if("S".equals(bcPoint.getResult())) {
			result.getBcPoint().setPoint(""+bcPoint.getAvail_point());
		}
		
        // Print results, including elapsed time
        logger.info("PointSwap Get card point total Elapsed time : " + (System.currentTimeMillis() - start));

		result.setUser_ci(param.getUser_ci());

		return result;
	}

	@Override
	public PointSwap swapCardPointsTest(HttpServletRequest request, PointSwap param) throws Exception {
		PointSwap result = new PointSwap();
		
        long start = System.currentTimeMillis();
        logger.info("PointSwap Use CALL : " + MessageUtil.getDateStr("hhmmss SSS") +" ============");
        
		//----------------- 전환 포인트 제한 체크를 위해 오늘 전환 포인트 조회 ------
//		PointSwapInfo tester = new PointSwapInfo();
//		tester.setUser_ci(param.getUser_ci());
//		tester.setTrans_req_date(MessageUtil.getDateStr("yyyyMMdd"));
//		Map<String, Integer> maxPoints = pointSwapDao.selectTodaySwapPoint(tester);
        Map<String, Object> tester = new HashMap<String, Object>();
        tester.put("user_ci", param.getUser_ci());
        tester.put("trans_req_date", MessageUtil.getDateStr("yyyyMMdd"));
        tester.put("trans_req_month", MessageUtil.getDateStr("yyyyMM"));
        
		List<Map<String, Object>> pointInfo = pointSwapDao.selectTodaySwapPoint(tester);
		int maxDayHanaMoney = 0;
		int maxMonthHanaMoney = 0;
		int maxDayHanaCnt = 0;
		int maxDayKbMoney = 0;
		int maxMonthKbMoney = 0;
		int maxDayShinhanMoney = 0;
		@SuppressWarnings("unused")
		int maxMonthShinhanMoney = 0;
		int maxDayBcMoney = 0;
		@SuppressWarnings("unused")
		int maxMonthBcMoney = 0;
		// [jaeheung] 2017.08.17 일일 전환 횟수 체크
		 
//		int maxHanaMoney = maxPoints.get("hanamembers") == null ? 0 : maxPoints.get("hanamembers");
//		int maxKbMoney = maxPoints.get("kbpointree") == null ? 0 : maxPoints.get("kbpointree");
//		int maxShinhanMoney = maxPoints.get("shinhan") == null ? 0 : maxPoints.get("shinhan");
		for(Map<String, Object> pointTemp : pointInfo) {
			String regSource = pointTemp.get("reg_source").toString();
			if(pointTemp.get("res_date_gb").toString().equals("M")) {
				switch(regSource) {
					case "hanamembers" : 
						maxMonthHanaMoney = pointTemp.get("req_point").toString() == null ? 0 : Integer.parseInt(pointTemp.get("req_point").toString());
						break;
					case "kbpointree" : 
						maxMonthKbMoney = pointTemp.get("req_point").toString() == null ? 0 : Integer.parseInt(pointTemp.get("req_point").toString());
						break;
					case "shinhan" : 
						maxMonthShinhanMoney = pointTemp.get("req_point").toString() == null ? 0 : Integer.parseInt(pointTemp.get("req_point").toString());
						break;
					case "bccard" : 
						maxMonthBcMoney = pointTemp.get("req_point").toString() == null ? 0 : Integer.parseInt(pointTemp.get("req_point").toString());
						break;
					default : 
						break;
				}
			} else {
				switch(regSource) {
					case "hanamembers" : 
						maxMonthHanaMoney = pointTemp.get("req_point").toString() == null ? 0 : Integer.parseInt(pointTemp.get("req_point").toString());
						maxDayHanaCnt = pointTemp.get("res_day_count").toString() == null ? 0 : Integer.parseInt(pointTemp.get("res_day_count").toString());
						break;
					case "kbpointree" : 
						maxMonthKbMoney = pointTemp.get("req_point").toString() == null ? 0 : Integer.parseInt(pointTemp.get("req_point").toString());
						break;
					case "shinhan" : 
						maxMonthShinhanMoney = pointTemp.get("req_point").toString() == null ? 0 : Integer.parseInt(pointTemp.get("req_point").toString());
						break;
					case "bccard" : 
						maxMonthBcMoney = pointTemp.get("req_point").toString() == null ? 0 : Integer.parseInt(pointTemp.get("req_point").toString());
						break;
					default : 
						break;
				}
			}
		}
		
        //----------------- 0일때는 전환 할 필요 없으므로 기준으로 삼기위해 미리 추출 ------
		int hanaMoney = parsePoint(param.getHanaPoint().getPoint());
        int kbMoney = parsePoint(param.getKbPoint().getPoint());
        int shinhanMoney = parsePoint(param.getShinhanPoint().getPoint());
        int bcMoney = parsePoint(param.getBcPoint().getPoint());
        
        logger.info("PointSwap Use request : hanaMoney = "+hanaMoney+", kbMoney = "+kbMoney+", shinhanMoney = "+shinhanMoney+", bcMoney = "+bcMoney);
        
        
        //----------------- 전송시 사용할 각각의 메시지 작성 ---------------------
		HanaCardMessage hanaMsg = new HanaCardMessage();
		hanaMsg.setUser_ci(param.getUser_ci());
		hanaMsg.setReq_point(hanaMoney);
		hanaMsg.setPointOver(maxMonthHanaMoney + hanaMoney > POINT_SWAP_MAX_HANA);
		
		if(maxDayHanaCnt >= POINT_SWAP_DAY_MAX_HANA) {
			hanaMsg.setCountOver(true);
		}
		
		KbCardMessage kbMsg = new KbCardMessage();
		kbMsg.setUser_ci(param.getUser_ci());
		kbMsg.setReq_point(kbMoney);
		logger.debug("month kb swap Point :: " + maxMonthKbMoney + kbMoney);
		kbMsg.setPointOver( maxMonthKbMoney + kbMoney > POINT_SWAP_MAX_KB);
		
		ShinhanCardMessage shinhanMsg = new ShinhanCardMessage();
		shinhanMsg.setUser_ci(param.getUser_ci());
		shinhanMsg.setCust_id_org(AES256CipherClip.AES_Decode(param.getCust_id()));
		shinhanMsg.setReq_point(shinhanMoney);
		shinhanMsg.setPointOver( maxDayShinhanMoney + shinhanMoney > POINT_SWAP_MAX_SHINHAN);
		
		BcCardMessage bcMsg = new BcCardMessage();
		bcMsg.setUser_ci(param.getUser_ci());
		bcMsg.setReq_point(bcMoney);
		
		// [jaeheung] 2017.08.11 일일 총한도 조회 위해 조회된 금일 카드사 전환 포인트 취합
		// TODO: bc는 총한도 없으므로 확인 필요.
		boolean isTotalPointOver = false;
		int maxTotal = (maxDayHanaMoney+hanaMoney) + (maxDayKbMoney+kbMoney) + (maxDayShinhanMoney+shinhanMoney) + (maxDayBcMoney+bcMoney);
  		if(POINT_SWAP_MAX_TOTAL <= maxTotal) {
  			hanaMsg.setPointOver(true);
  			kbMsg.setPointOver(true);
  			shinhanMsg.setPointOver(true);
  			bcMsg.setPointOver(true);
  			isTotalPointOver = true;
  		}
		
		
		//----------------- 비동기로 각각을 모두 호출 ---------------------
		Future<HanaCardMessage> hanaResult = hanaCardClientService.minusPoint(hanaMsg);
		Future<KbCardMessage> kbResult = kbCardClientService.minusPoint(kbMsg);
		Future<ShinhanCardMessage> shinhanResult = shinhanCardClientService.minusPoint(shinhanMsg);
		Future<BcCardMessage> bcResult = bcCardClientService.minusPoint(bcMsg);
		
		logger.debug("PointSwap Use WAIT : " + MessageUtil.getDateStr("hhmmss SSS") +" ============");

		//----------------- 결과를 취합 -----------------------------
		HanaCardMessage hanaPoint = hanaResult.get();
		KbCardMessage kbPoint = kbResult.get();
		ShinhanCardMessage shinhanPoint = shinhanResult.get();
		BcCardMessage bcPoint = bcResult.get();

		logger.info("PointSwap Use Response hana result["+hanaPoint.getResult()+"],resultCode["+hanaPoint.getRes_code()+"], avail point["+hanaPoint.getAvail_point() +"], use point["+hanaPoint.getRes_point()+"]");
		logger.info("PointSwap Use Response kb result["+kbPoint.getResult()+"],resultCode["+kbPoint.getRes_code()+"], avail point["+kbPoint.getAvail_point() +"], use point["+kbPoint.getRes_point()+"]");	
		logger.info("PointSwap Use Response shinhan result["+shinhanPoint.getResult()+"],resultCode["+shinhanPoint.getRes_code()+"], avail point["+shinhanPoint.getAvail_point() +"], use point["+shinhanPoint.getRes_point()+"]");
		logger.info("PointSwap Use Response bc result["+bcPoint.getResult()+"],resultCode["+bcPoint.getRes_code()+"], avail point["+bcPoint.getAvail_point() +"], use point["+bcPoint.getRes_point()+"]");
		
		logger.debug("PointSwap Use COMPLETE : " + MessageUtil.getDateStr("hhmmss SSS") +" ============");
		
		boolean totalCheck = false;
		

		//---------- 하나 멤버스 처리 결과 ------------------
		result.setHanaPoint(new CardPointInfo());
		result.getHanaPoint().setCardName("hanamembers");
		result.getHanaPoint().setResult(hanaPoint.getResult());
		result.getHanaPoint().setPointOver(hanaPoint.isPointOver());
		
		if(!hanaMsg.isPointOver() && hanaMsg.getReq_point() > 0 && !hanaMsg.isCountOver()) {
			if("S".equals(result.getHanaPoint().getResult())) {
				result.getHanaPoint().setPoint(""+hanaPoint.getRes_point());
				
				int ret = 0;	
				try {
					ret = clipPointService.procClipPoint(request, param, 0, 0, MessageUtil.getDateStr("yyyyMMdd")+hanaMsg.getTrans_id(), hanaMsg.getRes_point());		
				} catch (Exception e){
					e.printStackTrace();
					logger.error("하나 포인트 적립중 에러 발생", e);
					logger.info("PointSwap Use hanamembers Error [" + e.getMessage()+"]");
				}

				if(ret != 1) {
					PointSwapInfo cancelInfo = pointSwapDao.selectSwapListForTransId(hanaMsg);
					
					pointSwapDao.insertPointSwapCancelInfo(cancelInfo);
					logger.info("PointSwap Cancel Info Insert hanamembers old_swap_idx["+cancelInfo.getIdx()+"],reg_source["+cancelInfo.getReg_source()+"], reg_date["+cancelInfo.getTrans_req_date() +"]");
//					hanaCardClientService.cancelMinusPoint(hanaMsg);
					result.getHanaPoint().setResult("F");
					result.getHanaPoint().setResultMsg("포인트 전환에 실패하였습니다.");
					logger.info("PointSwap Use hanamembers Fail [" + result.getHanaPoint().getResultMsg()+"]");
				} else {
					totalCheck = true;
					logger.info("PointSwap Use hanamembers success");
				}
			} else {
				result.getHanaPoint().setResultMsg("포인트 전환에 실패하였습니다.");
				logger.info("PointSwap Use hanamembers Fail [" + result.getHanaPoint().getResultMsg()+"]");
			}
		}
		else if(hanaMsg.getReq_point() == 0) {
			// TODO
			result.getHanaPoint().setResult("N");
		}
		else if(hanaMsg.isCountOver()) {
			result.getHanaPoint().setResult("C");
			result.getHanaPoint().setResultMsg("일일 전환 횟수를 초과하였습니다.");
			logger.info("PointSwap Use hanamembers Fail [" + result.getHanaPoint().getResultMsg()+"]");
		}
		else {
			if(hanaMsg.isPointOver()) {
				result.getHanaPoint().setResult("F");
				result.getHanaPoint().setPointOver(true);
				result.getHanaPoint().setResultMsg("일일 전환 상한액을 초과하였습니다.");
				logger.info("PointSwap Use hanamembers Fail [" + result.getHanaPoint().getResultMsg()+"]");
			}
		}
		
		//---------- KB 처리 결과 ------------------
		result.setKbPoint(new CardPointInfo());
		result.getKbPoint().setCardName("kbpointree");
		result.getKbPoint().setResult(kbPoint.getResult());
		
		if(!kbMsg.isPointOver() && kbMsg.getReq_point() > 0) {
			if("S".equals(result.getKbPoint().getResult())) {
				result.getKbPoint().setPoint(""+kbPoint.getRes_point());
				
				int ret = 0;
				try {
					ret = clipPointService.procClipPoint(request, param, 1, 0, MessageUtil.getDateStr("yyyyMMdd")+kbMsg.getTrans_id(), kbMsg.getRes_point());
				} catch (Exception e){
					e.printStackTrace();
					logger.error("kb 포인트 적립중 에러 발생", e);
					logger.info("PointSwap Use kbpointree Error [" + e.getMessage()+"]");
				}

				if(ret != 1) {
//					kbCardClientService.cancelMinusPoint(kbMsg);
					PointSwapInfo cancelInfo = pointSwapDao.selectSwapListForTransId(kbMsg);
					
					pointSwapDao.insertPointSwapCancelInfo(cancelInfo);
					logger.info("PointSwap Cancel Info Insert kbpointree old_swap_idx["+cancelInfo.getIdx()+"],reg_source["+cancelInfo.getReg_source()+"], reg_date["+cancelInfo.getTrans_req_date() +"]");
					result.getKbPoint().setResult("F");
					result.getKbPoint().setResultMsg("포인트 전환에 실패하였습니다.");
					logger.info("PointSwap Use kbpointree Fail [" + result.getKbPoint().getResultMsg()+"]");
				} else {
					totalCheck = true;
					logger.info("PointSwap Use kbpointree success");
				}
				
				
			} else {
				result.getKbPoint().setResultMsg("포인트 전환에 실패하였습니다.");
				logger.info("PointSwap Use kbpointree Fail [" + result.getKbPoint().getResultMsg()+"]");
			}
		}
		else if(kbMsg.getReq_point() == 0) {
			// TODO
			result.getKbPoint().setResult("N");
		}
		else {
			if(kbMsg.isPointOver()) {
				result.getKbPoint().setResult("F");
				result.getKbPoint().setPointOver(true);
				result.getKbPoint().setResultMsg("일일 전환 상한액을 초과하였습니다.");
				logger.info("PointSwap Use kbpointree Fail [" + result.getKbPoint().getResultMsg()+"]");
			}
		}
		
		
		//---------- 신한  처리 결과 ------------------
		result.setShinhanPoint(new CardPointInfo());
		result.getShinhanPoint().setCardName("shinhan");
		result.getShinhanPoint().setResult(shinhanPoint.getResult());
		
		if(!shinhanMsg.isPointOver() && shinhanMsg.getReq_point() > 0) {
			if("S".equals(result.getShinhanPoint().getResult())) {
				result.getShinhanPoint().setPoint(""+shinhanPoint.getRes_point());
				
				int ret = 0;
				
				try {
					ret = clipPointService.procClipPoint(request, param, 2, 0, MessageUtil.getDateStr("yyyyMMdd")+shinhanMsg.getTrans_id(), shinhanMsg.getRes_point());
				} catch (Exception e){
					e.printStackTrace();
					logger.error("신한 포인트 적립중 에러 발생", e);
					logger.info("PointSwap Use Shinhan Error [" + e.getMessage()+"]");
				}

				if(ret != 1) {
//					shinhanCardClientService.cancelMinusPoint(shinhanMsg);
					PointSwapInfo cancelInfo = pointSwapDao.selectSwapListForTransId(shinhanMsg);
					
					pointSwapDao.insertPointSwapCancelInfo(cancelInfo);
					logger.info("PointSwap Cancel Info Insert Shinhan old_swap_idx["+cancelInfo.getIdx()+"],reg_source["+cancelInfo.getReg_source()+"], reg_date["+cancelInfo.getTrans_req_date() +"]");
					result.getShinhanPoint().setResult("F");
					result.getShinhanPoint().setResultMsg("포인트 전환에 실패하였습니다.");
					logger.info("PointSwap Use Shinhan Fail [" + result.getShinhanPoint().getResultMsg()+"]");
				} else {
					totalCheck = true;
					logger.info("PointSwap Use Shinhan pointswap success");
				}
				
			} else {
				result.getShinhanPoint().setResultMsg("포인트 전환에 실패하였습니다.");
				logger.info("PointSwap Use Shinhan Fail [" + result.getShinhanPoint().getResultMsg()+"]");
			}
		}
		else if(shinhanMsg.getReq_point() == 0) {
			// TODO
			result.getShinhanPoint().setResult("N");
		}
		else {
			if(shinhanMsg.isPointOver()){
				result.getShinhanPoint().setResult("F");
				result.getShinhanPoint().setPointOver(true);
				result.getShinhanPoint().setResultMsg("일일 전환 상한액을 초과하였습니다.");
				logger.info("PointSwap Use Shinhan Fail [" + result.getShinhanPoint().getResultMsg()+"]");
			}	
		}
		
		//---------- BC  카드  처리 결과 ------------------
		result.setBcPoint(new CardPointInfo());
		result.getBcPoint().setCardName("bccard");
		result.getBcPoint().setResult(bcPoint.getResult());
		
		if(!bcMsg.isPointOver() && bcMsg.getReq_point() > 0) {
			if("S".equals(result.getBcPoint().getResult())) {
				result.getBcPoint().setPoint(""+bcPoint.getRes_point());
				
				int ret = 0;
				
				try {
					ret = clipPointService.procClipPoint(request, param, 3, 0, MessageUtil.getDateStr("yyyyMMdd")+bcMsg.getTrans_id(), bcMsg.getRes_point());
				} catch (Exception e){
					e.printStackTrace();
					logger.error("비씨 포인트 적립중 에러 발생", e);
					logger.info("PointSwap Use BC Error [" + e.getMessage()+"]");
				}

				if(ret != 1) {
					PointSwapInfo cancelInfo = pointSwapDao.selectSwapListForTransId(bcMsg);
					
					pointSwapDao.insertPointSwapCancelInfo(cancelInfo);
					logger.info("PointSwap Cancel Info Insert BC old_swap_idx["+cancelInfo.getIdx()+"],reg_source["+cancelInfo.getReg_source()+"], reg_date["+cancelInfo.getTrans_req_date() +"]");
					result.getBcPoint().setResult("F");
					result.getBcPoint().setResultMsg("포인트 전환에 실패하였습니다.");
					logger.info("PointSwap Use BC Fail [" + result.getBcPoint().getResultMsg()+"]");
				} else {
					totalCheck = true;
					logger.info("PointSwap Use BC pointswap success");
				}
				
			} else {
				result.getBcPoint().setResultMsg("포인트 전환에 실패하였습니다.");
				logger.info("PointSwap Use BC Fail [" + result.getBcPoint().getResultMsg()+"]");
			}
		}
		else if(bcMsg.getReq_point() == 0) {
			// TODO
			result.getBcPoint().setResult("N");
		}
		else {
			if(bcMsg.isPointOver()){
				result.getBcPoint().setResult("F");
				result.getBcPoint().setPointOver(true);
				result.getBcPoint().setResultMsg("일일 전환 상한액을 초과하였습니다.");
				logger.info("PointSwap Use BC Fail [" + result.getBcPoint().getResultMsg()+"]");
			}	
		}
		
		
		if(!totalCheck) {
			result.setResult("F");
			//---------------------- [jaeheung] 2017.08.11 일일 총 상한액 초과시 분기 처리
			if(isTotalPointOver) {
				result.setResultMsg("일일 전환 총 상한포인트 " + POINT_SWAP_MAX_TOTAL + "을 초과하였습니다.");
				logger.info("PointSwap Use Fail [" + result.getResultMsg()+"]");
			} else {
				result.setResultMsg("포인트 전환에 실패하였습니다.");
				logger.info("PointSwap Use Fail [" + result.getResultMsg()+"]");
			}
		} else {
			result.setResult("S");
		}
		
        // Print results, including elapsed time
        logger.info("PointSwap Use Elapsed time: " + (System.currentTimeMillis() - start));

		result.setUser_ci(param.getUser_ci());

		return result;
	}
	
	private int parsePoint(String strMoney) {
		int ret = 0;
		try {
			ret = Integer.parseInt(strMoney);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return ret;
	}

	@Override
	public PointSwap isShinhanFan(HttpServletRequest request, PointSwap param) throws Exception {
		ShinhanCardMessage shinhanMsg = new ShinhanCardMessage();
		shinhanMsg.setUser_ci(param.getUser_ci());

		Future<ShinhanCardMessage> shinhanResult = shinhanCardClientService.shinhanFanCheck(shinhanMsg);
		ShinhanCardMessage shinhanPoint = shinhanResult.get();
		logger.debug("WAIT : " + MessageUtil.getDateStr("hhmmss SSS") +" ============");

		logger.debug(shinhanPoint.getResult());
		
		param.setResult(shinhanPoint.getResult());
		param.setResultMsg(shinhanPoint.getRes_message());
		
		return param;
	}

	@Override
	public int pointSwapCancelBatch() throws Exception {

		List<PointSwapCancelVo> cancelList = pointSwapDao.selectSwapCancelInfoList();
		logger.info("pointSwapCancelBatch targetList : "+cancelList.size());

		int counter = 0;
		for(PointSwapCancelVo data : cancelList) {
			
			int status = 2;
			try {
				// 등록된지 30초 내의 데이터는 다음으로 미룬다.
				if (!isOver30s(data)) {
					continue;
				}
				
				PointSwapInfo swapInfo = pointSwapDao.selectSwapHistForCancel(data.getOld_swap_idx());

				if(swapInfo == null) {
					data.setStatus(2);
					
				} else {					
					if("hanamembers".equals(swapInfo.getReg_source())){
						HanaCardMessage hanaMsg = new HanaCardMessage();
						hanaMsg.setUser_ci(swapInfo.getUser_ci());
						hanaMsg.setReq_point(swapInfo.getReq_point());
						hanaMsg.setPointOver(false);
						
						//취소를 다시 할때는 Ref_trans_id 를 tr_id로 전달
						if(TrType.USE.getValue().equals(swapInfo.getTrans_div()))
							hanaMsg.setTrans_id(swapInfo.getTrans_id());
						else
							hanaMsg.setTrans_id(swapInfo.getRef_trans_id());
						
						Future<HanaCardMessage> hanaResult = hanaCardClientService.cancelMinusPoint(hanaMsg);
						HanaCardMessage hanaPoint = hanaResult.get();
						if("S".equals(hanaPoint.getResult())) {
							status = 1;
						} else if (hanaPoint.getRes_code() == null || hanaPoint.getRes_code().equals("")) {
							// 타임아웃이면 다시 시도하도록 한다.
							status = 0;
						}
						data.setNew_swap_idx(hanaMsg.getIdx());
						
					} else if("shinhan".equals(swapInfo.getReg_source())){
						ShinhanCardMessage shinhanMsg = new ShinhanCardMessage();
						shinhanMsg.setUser_ci(swapInfo.getUser_ci());
						shinhanMsg.setCust_id_org(swapInfo.getCust_id_org() == null?"":swapInfo.getCust_id_org());
						shinhanMsg.setReq_point(swapInfo.getReq_point());
						shinhanMsg.setPointOver(false);
						
						//취소를 다시 할때는 Ref_trans_id 를 tr_id로 전달
						if(TrType.USE.getValue().equals(swapInfo.getTrans_div()))
							shinhanMsg.setTrans_id(swapInfo.getTrans_id());
						else
							shinhanMsg.setTrans_id(swapInfo.getRef_trans_id());
						
						Future<ShinhanCardMessage> shinhanResult = shinhanCardClientService.cancelMinusPoint(shinhanMsg);
						ShinhanCardMessage shinhanPoint = shinhanResult.get();
						if("S".equals(shinhanPoint.getResult())) {
							status = 1;
						} else if (shinhanPoint.getRes_code() == null || shinhanPoint.getRes_code().equals("")) {
							// 타임아웃이면 다시 시도하도록 한다.
							status = 0;
						}
						data.setNew_swap_idx(shinhanMsg.getIdx());
						
					} else if("kbpointree".equals(swapInfo.getReg_source())){
						KbCardMessage kbMsg = new KbCardMessage();
						kbMsg.setUser_ci(swapInfo.getUser_ci());
						kbMsg.setReq_point(swapInfo.getReq_point());
						kbMsg.setPointOver(false);
						
						//취소를 다시 할때는 Ref_trans_id 를 tr_id로 전달
						if(TrType.USE.getValue().equals(swapInfo.getTrans_div()))
							kbMsg.setTrans_id(swapInfo.getTrans_id());
						else
							kbMsg.setTrans_id(swapInfo.getRef_trans_id());
						
						Future<KbCardMessage> kbResult = kbCardClientService.cancelMinusPoint(kbMsg);
						KbCardMessage kbPoint = kbResult.get();
						if("S".equals(kbPoint.getResult())){
							status = 1;
						} else if (kbPoint.getRes_code() == null || kbPoint.getRes_code().equals("")) {
							// 타임아웃이면 다시 시도하도록 한다.
							status = 0;
						}
						
						data.setNew_swap_idx(kbMsg.getIdx());
					} else if("bccard".equals(swapInfo.getReg_source())){
						BcCardMessage bcMsg = new BcCardMessage();
						bcMsg.setUser_ci(swapInfo.getUser_ci());
						bcMsg.setReq_point(swapInfo.getReq_point());
						bcMsg.setApprove_no(swapInfo.getApprove_no());	// 비씨카드에서는 취소 시 필수값
						bcMsg.setPointOver(false);
						
						//취소를 다시 할때는 Ref_trans_id 를 tr_id로 전달
						if(TrType.USE.getValue().equals(swapInfo.getTrans_div()))
							bcMsg.setTrans_id(swapInfo.getTrans_id());
						else
							bcMsg.setTrans_id(swapInfo.getRef_trans_id());
						
						Future<BcCardMessage> bcResult = bcCardClientService.cancelMinusPoint(bcMsg);
						BcCardMessage bcPoint = bcResult.get();
						if("S".equals(bcPoint.getResult())) {
							status = 1;
						} else if (bcPoint.getRes_code() == null || bcPoint.getRes_code().equals("")) {
							// 타임아웃이면 다시 시도하도록 한다.
							status = 0;
						}
						data.setNew_swap_idx(bcMsg.getIdx());
					} else {
						logger.error("Invalid reg_source found!!!");
					}

				}

			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
			
			if(status == 1)
				counter++;
			
			data.setStatus(status);
			pointSwapDao.updateSwapCancelInfo(data);
		}
		
		logger.info("pointSwapCancelBatch End ("+counter+"/"+cancelList.size()+")");
		
		return counter;
	}
	
	private boolean isOver30s(PointSwapCancelVo data) {
		String date = data.getRegdate();
		int index = date.lastIndexOf(".");
		
		if (index >= 0) {
			date = date.substring(0, index);
		}
		
		Date regDate = DateUtil.getDate(date, "yyyy-MM-dd HH:mm:ss");
		
		if (new Date().getTime() - regDate.getTime() >= 30000) {
			return true;
		}
		
		return false;
	}

}
