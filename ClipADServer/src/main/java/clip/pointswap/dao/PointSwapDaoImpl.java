/*[CLiP Point] version [v3.0]
* Copyright © 2016 kt corp. All rights reserved.
* This is a proprietary software of kt corp, and you may not use this file except in
* compliance with license agreement with kt corp. Any redistribution or use of this
* software, with or without modification shall be strictly prohibited without prior written
* approval of kt corp, and the copyright notice above does not evidence any actual or
* intended publication of such software.
*/
package clip.pointswap.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.coopnc.cardpoint.domain.PointSwapInfo;

import clip.integration.bean.PointSwapCancelVo;

@Repository
public class PointSwapDaoImpl implements PointSwapDao {

	Logger log = Logger.getLogger(this.getClass());
	
	@Autowired
	private SqlSession sqlSession;

	@Override
	public int selectDailySerialNo(String systemCode) throws Exception {
		return sqlSession.selectOne("PointSwap.selectDailySerialNo", systemCode);
	}

	@Override
	public int selectDailyApproveNo(String systemCode) throws Exception {
		return sqlSession.selectOne("PointSwap.selectDailySerialNo", systemCode);
	}
	
	@Override
//	public List<Map<String, Object>> selectTodaySwapPoint(PointSwapInfo pointSwapInfo) throws Exception {
	public List<Map<String, Object>> selectTodaySwapPoint(Map<String, Object> pointSwapInfo) throws Exception {
		
		/* [jaeheung] 2017.08.17 쿼리의 반환 데이터 변경으로 인한 주석처리
		List<PointSwapInfo> dataList = sqlSession.selectList("PointSwap.selectTodaySwapPoint", pointSwapInfo); 
		
		Map<String, Integer> result = new HashMap<String, Integer>();
		for (PointSwapInfo data : dataList) {
			result.put(data.getReg_source(), data.getReq_point());
		}

		return dataList;
		 */
		
		List<Map<String, Object>> dataList = sqlSession.selectList("PointSwap.selectTodaySwapPoint", pointSwapInfo);
		
		return dataList;
		
	}
	
	@Override
	public List<PointSwapInfo> selectSwapList(PointSwapInfo pointSwapInfo) throws Exception {
		return sqlSession.selectList("PointSwap.selectSwapList", pointSwapInfo);
	}
	
	@Override
	public int insertPointSwapHist(PointSwapInfo pointSwapInfo) throws Exception {
		return sqlSession.insert("PointSwap.insertPointSwapHist", pointSwapInfo);
	}

	@Override
	public List<PointSwapCancelVo> selectSwapCancelInfoList() throws Exception {
		return sqlSession.selectList("PointSwap.selectSwapCancelInfoList", new PointSwapCancelVo());
	}

	@Override
	public void updateSwapCancelInfo(PointSwapCancelVo param) throws Exception {
		sqlSession.update("PointSwap.updateSwapCancelInfo", param);
	}

	@Override
	public PointSwapInfo selectSwapHistForCancel(int idx) throws Exception {
		return sqlSession.selectOne("PointSwap.selectSwapHistForCancel", idx);
	}

	@Override
	public int insertPointSwapCancelInfo(PointSwapInfo pointSwapInfo) throws Exception {
		return sqlSession.insert("PointSwap.insertPointSwapCancelInfo", pointSwapInfo);
	}

	@Override
	public PointSwapCancelVo selectSwapCancelInfo(int idx) throws Exception {
		return sqlSession.selectOne("PointSwap.selectSwapCancelInfo", idx);
	}
	
	@Override
	public PointSwapInfo selectSwapListForTransId(PointSwapInfo pointSwapInfo) throws Exception {
		return sqlSession.selectOne("PointSwap.selectSwapListForTransId", pointSwapInfo);
	}
	
	@Override
	public void updateSwapInfoApproveNo(PointSwapInfo param) throws Exception {
		sqlSession.update("PointSwap.updateSwapInfoApproveNo", param);
	}

	@Override
	public PointSwapInfo selectSwapInfo(PointSwapInfo pointSwapInfo) throws Exception {
		return sqlSession.selectOne("PointSwap.selectSwapInfo", pointSwapInfo);
	}
	
}
