/*[CLiP Point] version [v1.0]
* Copyright © 2016 kt corp. All rights reserved.
* This is a proprietary software of kt corp, and you may not use this file except in
* compliance with license agreement with kt corp. Any redistribution or use of this
* software, with or without modification shall be strictly prohibited without prior written
* approval of kt corp, and the copyright notice above does not evidence any actual or
* intended publication of such software.
*/
package clip.roulette.bean;

public class PointRoulette {

	private String selected_user; // 최고 당첨자 
	
	private String roulette_id;
	private String channelname;
	private String routype;
	private String area01;
	private String area02;
	private String area03;

	private String area04;
	private String area05;
	private String area06;
	private String point01;
	private String point02;
	private String point03;

	private String point04;
	private String point05;
	private String point06;
	private String max01;
	private String max02;
	private String max03;
	private String max04;

	private String max05;
	private String max06;
	private String totmax;
	private String timegbn;
	private String starttime;
	private String endtime;

	private String timenum;
	private String startdate;
	private String enddate;
	private String status; // 0:중지 1:사용
	private String userid;
	private String ipaddr;

	private String regdate;
	private String modiipaddr;
	private String modidate;
	
	 
	 private String desc01;
	 private String desc02;
	 private String imagepath;
	 //private String requester_code;
	 //private String requester_description;
	
	public String getSelected_user() {
		return selected_user;
	}
	public void setSelected_user(String selected_user) {
		this.selected_user = selected_user;
	}
	public String getRoulette_id() {
		return roulette_id;
	}
	public void setRoulette_id(String roulette_id) {
		this.roulette_id = roulette_id;
	}
	public String getChannelname() {
		return channelname;
	}
	public void setChannelname(String channelname) {
		this.channelname = channelname;
	}
	public String getRoutype() {
		return routype;
	}
	public void setRoutype(String routype) {
		this.routype = routype;
	}
	public String getArea01() {
		return area01;
	}
	public void setArea01(String area01) {
		this.area01 = area01;
	}
	public String getArea02() {
		return area02;
	}
	public void setArea02(String area02) {
		this.area02 = area02;
	}
	public String getArea03() {
		return area03;
	}
	public void setArea03(String area03) {
		this.area03 = area03;
	}
	public String getArea04() {
		return area04;
	}
	public void setArea04(String area04) {
		this.area04 = area04;
	}
	public String getArea05() {
		return area05;
	}
	public void setArea05(String area05) {
		this.area05 = area05;
	}
	public String getArea06() {
		return area06;
	}
	public void setArea06(String area06) {
		this.area06 = area06;
	}
	public String getPoint01() {
		return point01;
	}
	public void setPoint01(String point01) {
		this.point01 = point01;
	}
	public String getPoint02() {
		return point02;
	}
	public void setPoint02(String point02) {
		this.point02 = point02;
	}
	public String getPoint03() {
		return point03;
	}
	public void setPoint03(String point03) {
		this.point03 = point03;
	}
	public String getPoint04() {
		return point04;
	}
	public void setPoint04(String point04) {
		this.point04 = point04;
	}
	public String getPoint05() {
		return point05;
	}
	public void setPoint05(String point05) {
		this.point05 = point05;
	}
	public String getPoint06() {
		return point06;
	}
	public void setPoint06(String point06) {
		this.point06 = point06;
	}
	public String getMax01() {
		return max01;
	}
	public void setMax01(String max01) {
		this.max01 = max01;
	}
	public String getMax02() {
		return max02;
	}
	public void setMax02(String max02) {
		this.max02 = max02;
	}
	public String getMax03() {
		return max03;
	}
	public void setMax03(String max03) {
		this.max03 = max03;
	}
	public String getMax04() {
		return max04;
	}
	public void setMax04(String max04) {
		this.max04 = max04;
	}
	public String getMax05() {
		return max05;
	}
	public void setMax05(String max05) {
		this.max05 = max05;
	}
	public String getMax06() {
		return max06;
	}
	public void setMax06(String max06) {
		this.max06 = max06;
	}
	public String getTotmax() {
		return totmax;
	}
	public void setTotmax(String totmax) {
		this.totmax = totmax;
	}
	public String getTimegbn() {
		return timegbn;
	}
	public void setTimegbn(String timegbn) {
		this.timegbn = timegbn;
	}
	public String getStarttime() {
		return starttime;
	}
	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}
	public String getEndtime() {
		return endtime;
	}
	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}
	public String getTimenum() {
		return timenum;
	}
	public void setTimenum(String timenum) {
		this.timenum = timenum;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}
	public String getIpaddr() {
		return ipaddr;
	}
	public void setIpaddr(String ipaddr) {
		this.ipaddr = ipaddr;
	}
	public String getRegdate() {
		return regdate;
	}
	public void setRegdate(String regdate) {
		this.regdate = regdate;
	}
	public String getModiipaddr() {
		return modiipaddr;
	}
	public void setModiipaddr(String modiipaddr) {
		this.modiipaddr = modiipaddr;
	}
	public String getModidate() {
		return modidate;
	}
	public void setModidate(String modidate) {
		this.modidate = modidate;
	}
	public String getDesc01() {
		return desc01;
	}
	public void setDesc01(String desc01) {
		this.desc01 = desc01;
	}
	public String getDesc02() {
		return desc02;
	}
	public void setDesc02(String desc02) {
		this.desc02 = desc02;
	}
	public String getImagepath() {
		return imagepath;
	}
	public void setImagepath(String imagepath) {
		this.imagepath = imagepath;
	}

}
