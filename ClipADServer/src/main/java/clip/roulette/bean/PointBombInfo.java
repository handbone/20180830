package clip.roulette.bean;

public class PointBombInfo {
	
	private String result_code;
	
	// 화면에 결과 보내주기용
	private String tr_id;
	private String result;
	private String resultMsg;
	
	private String landing_url;
	private String ga_id;
	private String user_ci;
	private String ctn;
	
	// point_bomb_info
	private String cust_id;

	private String base_date;
	private String use_yn;
	private String use_start_date;
	private String direct_yn;
	private String banner_id;
	private int base_point;
	private int real_base_point;
	
	private int level;
	private int reward_point; 	//최종 버전에서는 무시함.
	private int reward_level;
	private int reward_base;
	private int bomb_count;
	private String regdate;
	private String modidate;

	// point_bomb_day_info
	private int day_count;
	private String day_content;
	private String day_link;
	private String button_expose_yn;
	
	public String getCust_id() {
		return cust_id;
	}
	public void setCust_id(String cust_id) {
		this.cust_id = cust_id;
	}
	public String getBase_date() {
		return base_date;
	}
	public void setBase_date(String base_date) {
		this.base_date = base_date;
	}
	public String getUse_yn() {
		return use_yn;
	}
	public void setUse_yn(String use_yn) {
		this.use_yn = use_yn;
	}
	public String getUse_start_date() {
		return use_start_date;
	}
	public void setUse_start_date(String use_start_date) {
		this.use_start_date = use_start_date;
	}
	public String getDirect_yn() {
		return direct_yn;
	}
	public void setDirect_yn(String direct_yn) {
		this.direct_yn = direct_yn;
	}
	public String getBanner_id() {
		return banner_id;
	}
	public void setBanner_id(String banner_id) {
		this.banner_id = banner_id;
	}
	public int getBase_point() {
		return base_point;
	}
	public void setBase_point(int base_point) {
		this.base_point = base_point;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public String getRegdate() {
		return regdate;
	}
	public void setRegdate(String regdate) {
		this.regdate = regdate;
	}
	public String getModidate() {
		return modidate;
	}
	public void setModidate(String modidate) {
		this.modidate = modidate;
	}
	public int getDay_count() {
		return day_count;
	}
	public void setDay_count(int day_count) {
		this.day_count = day_count;
	}
	public String getDay_content() {
		return day_content;
	}
	public void setDay_content(String day_content) {
		this.day_content = day_content;
	}
	public String getDay_link() {
		return day_link;
	}
	public void setDay_link(String day_link) {
		this.day_link = day_link;
	}
	public String getButton_expose_yn() {
		return button_expose_yn;
	}
	public void setButton_expose_yn(String button_expose_yn) {
		this.button_expose_yn = button_expose_yn;
	}
	public int getBomb_count() {
		return bomb_count;
	}
	public void setBomb_count(int bomb_count) {
		this.bomb_count = bomb_count;
	}
	public String getResult_code() {
		return result_code;
	}
	public void setResult_code(String result_code) {
		this.result_code = result_code;
	}
	public String getResultMsg() {
		return resultMsg;
	}
	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public int getReward_point() {
		return reward_point;
	}
	public void setReward_point(int reward_point) {
		this.reward_point = reward_point;
	}
	public int getReward_level() {
		return reward_level;
	}
	public void setReward_level(int reward_level) {
		this.reward_level = reward_level;
	}
	public int getReward_base() {
		return reward_base;
	}
	public void setReward_base(int reward_base) {
		this.reward_base = reward_base;
	}
	public String getLanding_url() {
		return landing_url;
	}
	public void setLanding_url(String landing_url) {
		this.landing_url = landing_url;
	}
	public String getGa_id() {
		return ga_id;
	}
	public void setGa_id(String ga_id) {
		this.ga_id = ga_id;
	}
	public int getReal_base_point() {
		return real_base_point;
	}
	public void setReal_base_point(int real_base_point) {
		this.real_base_point = real_base_point;
	}
	public String getUser_ci() {
		return user_ci;
	}
	public void setUser_ci(String user_ci) {
		this.user_ci = user_ci;
	}
	public String getCtn() {
		return ctn;
	}
	public void setCtn(String ctn) {
		this.ctn = ctn;
	}
	public String getTr_id() {
		return tr_id;
	}
	public void setTr_id(String tr_id) {
		this.tr_id = tr_id;
	}

}
