package clip.roulette.bean;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BuzzvilMessage {
	
	private int code;
	private String msg;
	private String landing_url;
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getLanding_url() {
		return landing_url;
	}
	public void setLanding_url(String landing_url) {
		this.landing_url = landing_url;
	}
}