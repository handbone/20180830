/*[CLiP Point] version [v1.0]
* Copyright © 2016 kt corp. All rights reserved.
* This is a proprietary software of kt corp, and you may not use this file except in
* compliance with license agreement with kt corp. Any redistribution or use of this
* software, with or without modification shall be strictly prohibited without prior written
* approval of kt corp, and the copyright notice above does not evidence any actual or
* intended publication of such software.
*/
package clip.sample.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import clip.sample.service.SampleServiceImpl;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import clip.framework.BaseConstant;
import clip.framework.HttpNetwork;


@Controller
@RequestMapping("/sample")
public class SampleController {
	Logger log = Logger.getLogger(this.getClass());
	
	@Resource(name="sampleService")
	private SampleServiceImpl sampleService;
	
	@Autowired
	private HttpNetwork httpNetwork;
	
	@RequestMapping(value="/openSampleBoardList.do")
	public ModelAndView openSampleBoardList(Map<String,Object> commandMap) throws java.lang.Exception{

		ModelAndView mv = new ModelAndView("/sample/boardList");
		
		List<Map<String,Object>> list = sampleService.selectBoardList(commandMap);
		mv.addObject("list", list);
		
		return mv;
	}
	
	@RequestMapping(value="/pointTest.do")
    public ModelAndView pointTest(HttpServletRequest request, Model model) throws java.lang.Exception{
		ModelAndView mv = new ModelAndView("/sample/point_use_save");
    	return mv;
    }
	
	@RequestMapping(value="/saveOrUse.do", method = { RequestMethod.POST })
	@ResponseBody
    public String saveOrUse(HttpServletRequest request, Model mode) throws java.lang.Exception{
    	
    	String cust_id = (String) request.getParameter("cust_id");
    	String action = (String) request.getParameter("action");
    	String desc = (String) request.getParameter("desc");
    	String point = (String) request.getParameter("point");
    	
    	String tr_id = String.valueOf(System.currentTimeMillis());
    	
    	StringBuffer sb = new StringBuffer(tr_id);
    	
		if (tr_id.length() < 16) {
			while (sb.length() < 16) {
				sb.append("0");
			}
		}
		tr_id = sb.toString();
		
		String strDo ="";
		String endUrl = "";
		String strHtmlSource = "";
		String user_ci = "";
		
		if(cust_id.equals("1jtXDc8/vDbcVNNKqAHsOg==")){
			user_ci = "1jtXDc8";
		}else if(cust_id.equals("2jtXDc8/vDbcVNNKqAHsOg==")){
			user_ci = "2jtXDc8";
		}else if(cust_id.equals("3jtXDc8/vDbcVNNKqAHsOg==")){
			user_ci = "3jtXDc8";
		}else if(cust_id.equals("4jtXDc8/vDbcVNNKqAHsOg==")){
			user_ci = "4jtXDc8";
		}else if(cust_id.equals("6jtXDc8/vDbcVNNKqAHsOg==")){
			user_ci = "6jtXDc8";
		}else if(cust_id.equals("7jtXDc8/vDbcVNNKqAHsOg==")){
			user_ci = "7jtXDc8";
		}else if(cust_id.equals("8jtXDc8/vDbcVNNKqAHsOg==")){
			user_ci = "8jtXDc8";
		}else if(cust_id.equals("9jtXDc8/vDbcVNNKqAHsOg==")){
			user_ci = "9jtXDc8";
		}else if(cust_id.equals("0jtXDc8/vDbcVNNKqAHsOg==")){
			user_ci = "0jtXDc8";
		}else if(cust_id.equals("16551651")){
			user_ci = "1DbcVNNKqAHsOg==";
		}else if(cust_id.equals("16551652")){
			user_ci = "2DbcVNNKqAHsOg==";
		}else if(cust_id.equals("16551653")){
			user_ci = "3DbcVNNKqAHsOg==";
		}else if(cust_id.equals("16551654")){
			user_ci = "4DbcVNNKqAHsOg==";
		}else if(cust_id.equals("16551655")){
			user_ci = "5DbcVNNKqAHsOg==";
		}else if(cust_id.equals("16551656")){
			user_ci = "6DbcVNNKqAHsOg==";
		}else if(cust_id.equals("16551657")){
			user_ci = "7DbcVNNKqAHsOg==";
		}else if(cust_id.equals("16551658")){
			user_ci = "8DbcVNNKqAHsOg==";
		}else if(cust_id.equals("16551659")){
			user_ci = "9DbcVNNKqAHsOg==";
		}else{
			user_ci = "8jtXDc8/vDbcVNNKqAHsOg==";
		}
		
    	if(action.equals("save")){
    		strDo = BaseConstant.PLUS_POINT;
    		endUrl = "cust_id="+cust_id
					+"&transaction_id="+tr_id
					+"&requester_code="+"clippoint"
					+"&point_value="+point
					+"&description="+java.net.URLEncoder.encode(desc,BaseConstant.DEFAULT_ENCODING); 
    		
    		
    	}else{
    		
    		strDo = "minusPoint.do";
    		endUrl = "user_ci="+user_ci
					+"&transaction_id="+tr_id
					+"&requester_code="+"clippoint"
					+"&point_value="+point
					+"&description="+java.net.URLEncoder.encode(desc,BaseConstant.DEFAULT_ENCODING); 
    	}
    	
    	try{
			strHtmlSource = httpNetwork.strGetData(strDo,endUrl);
			log.debug("strHtmlSource:::::"+strHtmlSource);
			strHtmlSource = "success";
		}catch (Exception e ){
			log.debug("Exception e :::::"+e.getMessage());
			strHtmlSource = "failure";
			
		}
    	
    	Map<String,String> map = new HashMap<String,String>();
		
		map.put("result", strHtmlSource);
		return JSONValue.toJSONString(map);
		
    	
    }
	
}
