package clip.integration.dao;

import java.util.Map;

import clip.mypoint.bean.CouponState;

/**
 * 쿠폰 등록 결과 히스토리 DAO 인터페이스
 * 
 * @author jaeheung
 * @date 2017.08.07
 */
public interface PointCouponDao {
	
	/**
	 * 쿠폰 등록 결과 히스토리 추가
	 * 
	 * @author jaeheung
	 * @date 2017.08.07
	 * @param msg - 쿠폰 등록 결과 정보
	 */
	public void insertCouponHistory(Map<String, Object> msg);

	public int insertCouponBlock(String user_ci);

	public int updateCouponBlock(CouponState obj);

	public CouponState selectFailCouponCheck(String user_ci);

}
