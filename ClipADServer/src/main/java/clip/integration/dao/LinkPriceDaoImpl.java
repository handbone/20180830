package clip.integration.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import clip.integration.bean.LinkPriceItem;

@Repository
public class LinkPriceDaoImpl implements LinkPriceDao {
	Logger log = Logger.getLogger(this.getClass());
	
	@Autowired
	private SqlSession sqlSession;

	@Override
	public List<LinkPriceItem> selectItemList() {
		return sqlSession.selectList("LinkPrice.selectItemList", null);
	}

	@Override
	public LinkPriceItem selectItem(LinkPriceItem param) {
		return sqlSession.selectOne("LinkPrice.selectItem", param);
	}

	
}
