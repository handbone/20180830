package clip.integration.bean;

import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClipContract2Message {
	
	//약정그룹
	public static final String GROUP_POINT_SWAP			= "point_swap";
	
	//포인트전환 - 카드아이템
	public static final String ITEM_HANAMEMBERS			= "hanamembers";
	public static final String ITEM_SHINHANCARD			= "shinhancard";
	public static final String ITEM_KBCARD				= "kbcard";
	public static final String ITEM_BCCARD				= "pointswap_bc";
	
	//web, 서버간 통신 관련
	public String serviceName;
	public String result;
	public String resultMsg;
	public String contractShowYn;
	
	//약관 그룹코드
	private String groupCode;
	//사용자 cust_id
	private String custId;
	//서비스 아이디
	private String linkServiceId = "clippoint";
	//유효시간 (현재 + 30분)
	private String expired;
	//보안 강화에 따른 정보 변조 유무 체크를 위한 전송 정보 문자열 hash 값
	//sign = HexEncode-lowcase(HmacSHA256(expired + custId + groupCode + linkServiceId + "terms" + "agreelist" + "get", 
	//			custId + groupCode + '0000..." (32자))
	//단, 이때 요소값은 원문으로 조홥 (암호화가 안된 평문)
	private String sign;

	//응답코드 OK-정상, FAIL-오류
	private String retcode;
	//응답메시지 SUCCESS-정상, 나머지 오류
	private String retmsg;
	//오류코드
	private String code;
	//처리 ID
	private String txId;
	
	//약정목록
	private List<ClipContractItem> termList;
	
	public static class ClipContractItem {
		private String groupCode;
		private String itemCode;
		private String version;
		private String itemName;
		private String mandatoryYN;
		private String userAgreeYN;
		private String userAgreeDateTime;

		public String getGroupCode() {
			return groupCode;
		}
		public void setGroupCode(String groupCode) {
			this.groupCode = groupCode;
		}
		public String getItemCode() {
			return itemCode;
		}
		public void setItemCode(String itemCode) {
			this.itemCode = itemCode;
		}
		public String getVersion() {
			return version;
		}
		public void setVersion(String version) {
			this.version = version;
		}
		public String getItemName() {
			return itemName;
		}
		public void setItemName(String itemName) {
			this.itemName = itemName;
		}
		public String getMandatoryYN() {
			return mandatoryYN;
		}
		public void setMandatoryYN(String mandatoryYN) {
			this.mandatoryYN = mandatoryYN;
		}
		public String getUserAgreeYN() {
			return userAgreeYN;
		}
		public void setUserAgreeYN(String userAgreeYN) {
			this.userAgreeYN = userAgreeYN;
		}
		public String getUserAgreeDateTime() {
			return userAgreeDateTime;
		}
		public void setUserAgreeDateTime(String userAgreeDateTime) {
			this.userAgreeDateTime = userAgreeDateTime;
		}
	}

	public String getGroupCode() {
		return groupCode;
	}
	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}
	public String getCustId() {
		return custId;
	}
	public void setCustId(String custId) {
		this.custId = custId;
	}
	public String getLinkServiceId() {
		return linkServiceId;
	}
	public void setLinkServiceId(String linkServiceId) {
		this.linkServiceId = linkServiceId;
	}
	public String getExpired() {
		return expired;
	}
	public void setExpired(String expired) {
		this.expired = expired;
	}
	public String getSign() {
		return sign;
	}
	public void setSign(String sign) {
		this.sign = sign;
	}
	public String getRetcode() {
		return retcode;
	}
	public void setRetcode(String retcode) {
		this.retcode = retcode;
	}
	public String getRetmsg() {
		return retmsg;
	}
	public void setRetmsg(String retmsg) {
		this.retmsg = retmsg;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getTxId() {
		return txId;
	}
	public void setTxId(String txId) {
		this.txId = txId;
	}

	public String getParameters() {
		StringBuffer sb = new StringBuffer();
		sb.append("?groupCode=");
		sb.append(this.groupCode);
		sb.append("&custId=");
		sb.append(this.custId);
		sb.append("&linkServiceId=");
		sb.append(this.linkServiceId);
		sb.append("&expired=");
		sb.append(this.expired);
		sb.append("&sign=");
		sb.append(this.sign);
		return sb.toString();
	}
	public List<ClipContractItem> getTermList() {
		return termList;
	}
	public void setTermList(List<ClipContractItem> termList) {
		this.termList = termList;
	}
	public String getServiceName() {
		return serviceName;
	}
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getResultMsg() {
		return resultMsg;
	}
	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}
	public String getContractShowYn() {
		return contractShowYn;
	}
	public void setContractShowYn(String contractShowYn) {
		this.contractShowYn = contractShowYn;
	}

}
