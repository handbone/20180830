package clip.integration.bean;

public class ClipPointMessage {
	
	private String reg_source;	//requester_code
	private String user_ci;
	private String user_token;
	private String cust_id;
	private String ga_id;
	private String ctn;
	
	private String tr_id;
	private int point;
	private String description;

	private String result;
	private String resultMsg;

	//응답 데이터 추가
	private String transaction_id;
	private String requester_code;
	private String approve_no;
	private String datetime;
	private String point_value;
	private String balance;
	
	private String reference_id;

	public String getReg_source() {
		return reg_source;
	}

	public void setReg_source(String reg_source) {
		this.reg_source = reg_source;
	}

	public String getCust_id() {
		return cust_id;
	}

	public void setCust_id(String cust_id) {
		this.cust_id = cust_id;
	}

	public String getGa_id() {
		return ga_id;
	}

	public void setGa_id(String ga_id) {
		this.ga_id = ga_id;
	}

	public String getCtn() {
		return ctn;
	}

	public void setCtn(String ctn) {
		this.ctn = ctn;
	}

	public String getUser_ci() {
		return user_ci;
	}

	public void setUser_ci(String user_ci) {
		this.user_ci = user_ci;
	}

	public String getTr_id() {
		return tr_id;
	}

	public void setTr_id(String tr_id) {
		this.tr_id = tr_id;
	}

	public String getUser_token() {
		return user_token;
	}

	public void setUser_token(String user_token) {
		this.user_token = user_token;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static enum RegSource {
		
		CLIP_POINT("clippoint"),
		HANAMEMBERS("hanamembers"),
		SHINHANCARD("shinhancard"),
		KBCARD("kbcard"),
		BCCARD("bccard");
    
        private String value;
        
        RegSource (String value){
        	this.value = value;
        }
        
        public String getValue(){
        	return this.value;
        }
        
        public static RegSource getEnum(String code){
            for(RegSource e : RegSource.values()){
                if(code == e.value) return e;
            }
            return null;
        }
    }

	public static enum RegType {
		
		GET_POINT_HISTORY("/adv/getPointHistory.do"),
		GET_POINT("/adv/getPoint.do"),
		GET_POINT_REFRESH("/adv/getPointRefresh.do"),
		PLUS_POINT("/adv/plusPoint.do"),
		PLUS_POINT_CANCEL("/adv/plusPointCancel.do"),
		MINUS_POINT("/adv/minusPoint.do"),
		MINUS_POINT_CANCEL("/adv/minusPointCancel.do"),
    
		OLD_GET_POINT_HISTORY("/getPointHistory.do"),
		OLD_GET_POINT("/getPoint.do"),
		OLD_GET_POINT_REFRESH("/getPointRefresh.do"),
		OLD_PLUS_POINT("/plusPoint.do"),
		OLD_PLUS_POINT_CANCEL("/plusPointCancel.do"),
		OLD_MINUS_POINT("/minusPoint.do"),
		OLD_MINUS_POINT_CANCEL("/minusPointCancel.do");
		
        private String value;
        
        RegType (String value){
        	this.value = value;
        }
        
        public String getValue(){
        	return this.value;
        }
        
        public static RegType getEnum(String code){
            for(RegType e : RegType.values()){
                if(code == e.value) return e;
            }
            return null;
        }
    }

	public String getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}

	public String getRequester_code() {
		return requester_code;
	}

	public void setRequester_code(String requester_code) {
		this.requester_code = requester_code;
	}

	public String getApprove_no() {
		return approve_no;
	}

	public void setApprove_no(String approve_no) {
		this.approve_no = approve_no;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public String getPoint_value() {
		return point_value;
	}

	public void setPoint_value(String point_value) {
		this.point_value = point_value;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getResult() {
		return result;
	}

	public String getReference_id() {
		return reference_id;
	}

	public void setReference_id(String reference_id) {
		this.reference_id = reference_id;
	}

	public int getPoint() {
		return point;
	}

	public void setPoint(int point) {
		this.point = point;
	}

	public String getResultMsg() {
		return resultMsg;
	}

	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}
	
}
