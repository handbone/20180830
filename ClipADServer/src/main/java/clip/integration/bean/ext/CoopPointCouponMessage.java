package clip.integration.bean.ext;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="AUTH_WEB_01")
@XmlAccessorType(XmlAccessType.FIELD)
public class CoopPointCouponMessage {
	
	private String couponNo;
	
	//자체 코드
	private String result;
	private String resultMsg;

	//망취소용 원거래 tr_id
	private String tr_id;

	//결과코드
	//성공
	public static final String RET_SUCCESS = "00";
	//거래번호 중복요청 오류
	public static final String RET_NOT_ESXIST_ERR = "01";	//01	미존재쿠폰
	public static final String RET_IVALID_COUPON_ERR = "02";		//02	비정상쿠폰번호
	public static final String RET_PERIOD_END_ERR = "04";	//04	기간만료쿠폰
	public static final String RET_PAYED_ERR = "05";		//05	사용된 쿠폰
	public static final String RET_CANCEL_ERR = "06";		//06	결제취소 쿠폰

	public static final String RET_PRODUCT_CODE_ERR_1 = "80";	//80 상품 종류 코드 불일치 오류
	public static final String RET_PRODUCT_CODE_ERR_2 = "81";	//81 상품 종류 코드 불일치 오류
	public static final String RET_PRODUCT_CODE_ERR_3 = "84";	//84 상품 종류 코드 불일치 오류

	
	@XmlElement(name = "RESULTCODE")
	private String resultcode;
	@XmlElement(name = "RESULTMSG")
	private String resultmsg;
	@XmlElement(name = "COMMAND")
	private String command;
	@XmlElement(name = "COUPON_NAME")
	private String coupon_name;
	@XmlElement(name = "USE_PRICE")
	private int use_price;
	@XmlElement(name = "PRODUCT_CODE")
	private String product_code;
	@XmlElement(name = "BAL_PRICE")
	private int bal_price;
	@XmlElement(name = "AUTH_COUNT")
	private String auth_count;
	@XmlElement(name = "COUPON_TYPE")
	private String coupon_type;
	@XmlElement(name = "USE_YN")
	private String use_yn;
	@XmlElement(name = "BRANCE_CODE")
	private String brance_code;
	@XmlElement(name = "AUTH_CODE")
	private String auth_code;
	@XmlElement(name = "USE_DATE")
	private String use_date;
	@XmlElement(name = "SEL_PRICE")
	private String sel_price;
	@XmlElement(name = "GW_CODE")
	private String gw_code;
	@XmlElement(name = "START_DAY")
	private String start_day;
	@XmlElement(name = "END_DAY")
	private String end_day;

	
	public String getResultcode() {
		return resultcode;
	}
	public void setResultcode(String resultcode) {
		this.resultcode = resultcode;
	}
	public String getResultmsg() {
		return resultmsg;
	}
	public void setResultmsg(String resultmsg) {
		this.resultmsg = resultmsg;
	}
	public String getCommand() {
		return command;
	}
	public void setCommand(String command) {
		this.command = command;
	}
	public String getCoupon_name() {
		return coupon_name;
	}
	public void setCoupon_name(String coupon_name) {
		this.coupon_name = coupon_name;
	}
	public String getProduct_code() {
		return product_code;
	}
	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}
	public String getAuth_count() {
		return auth_count;
	}
	public void setAuth_count(String auth_count) {
		this.auth_count = auth_count;
	}
	public String getCoupon_type() {
		return coupon_type;
	}
	public void setCoupon_type(String coupon_type) {
		this.coupon_type = coupon_type;
	}
	public String getUse_yn() {
		return use_yn;
	}
	public void setUse_yn(String use_yn) {
		this.use_yn = use_yn;
	}
	public String getBrance_code() {
		return brance_code;
	}
	public void setBrance_code(String brance_code) {
		this.brance_code = brance_code;
	}
	public String getAuth_code() {
		return auth_code;
	}
	public void setAuth_code(String auth_code) {
		this.auth_code = auth_code;
	}
	public String getUse_date() {
		return use_date;
	}
	public void setUse_date(String use_date) {
		this.use_date = use_date;
	}
	public String getSel_price() {
		return sel_price;
	}
	public void setSel_price(String sel_price) {
		this.sel_price = sel_price;
	}
	public String getGw_code() {
		return gw_code;
	}
	public void setGw_code(String gw_code) {
		this.gw_code = gw_code;
	}
	public String getStart_day() {
		return start_day;
	}
	public void setStart_day(String start_day) {
		this.start_day = start_day;
	}
	public String getEnd_day() {
		return end_day;
	}
	public void setEnd_day(String end_day) {
		this.end_day = end_day;
	}
	public String getResultMsg() {
		return resultMsg;
	}
	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getTr_id() {
		return tr_id;
	}
	public void setTr_id(String tr_id) {
		this.tr_id = tr_id;
	}
	public String getCouponNo() {
		return couponNo;
	}
	public void setCouponNo(String couponNo) {
		this.couponNo = couponNo;
	}
	public int getUse_price() {
		return use_price;
	}
	public void setUse_price(int use_price) {
		this.use_price = use_price;
	}
	public int getBal_price() {
		return bal_price;
	}
	public void setBal_price(int bal_price) {
		this.bal_price = bal_price;
	}

}
