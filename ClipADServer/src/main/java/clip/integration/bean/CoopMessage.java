package clip.integration.bean;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="CREATECOUPON_OUT_FILLER")
@XmlAccessorType(XmlAccessType.FIELD)
public class CoopMessage {

	//결과코드
	//성공
	public static final String RET_SUCCESS = "00";
	//거래번호 중복요청 오류
	public static final String RET_DUP     = "71";

	/*private String goodId;
	private String trId;
	private String filler01;
	private String filler02;
	private String filler03;
	private String filler;

	private String retCode;
	private String retMsg;
	private String couponId;
	private String pinId;*/
	
	
	@XmlElement(name = "COUPONCODE")
	private String goodId;
	
	@XmlElement(name = "SEQNUMBER")
	private String trId;
	
	@XmlElement(name = "FILLER01")
	private String filler01;
	
	@XmlElement(name = "FILLER02")
	private String filler02;
	
	@XmlElement(name = "FILLER03")
	private String filler03;
	
	@XmlElement(name = "FILLER")
	private String filler;
	
	@XmlElement(name = "RESULTCODE")
	private String retCode;
	
	@XmlElement(name = "RESULTMSG")
	private String retMsg;
	
	@XmlElement(name = "COUPONNUMBER")
	private String couponId;
	
	@XmlElement(name = "PINNUMBER")
	private String pinId;
	
	@XmlElement(name = "CODE")
	private String code;
	
	@XmlElement(name = "PASS")
	private String pass;
	
	//처리 결과
	private String result;
	private String resultMsg;
	private String barcodeImgUrl;

	public String getGoodId() {
		return goodId;
	}
	public void setGoodId(String goodId) {
		this.goodId = goodId;
	}
	public String getTrId() {
		return trId;
	}
	public void setTrId(String trId) {
		this.trId = trId;
	}
	public String getFiller01() {
		return filler01;
	}
	public void setFiller01(String filler01) {
		this.filler01 = filler01;
	}
	public String getFiller02() {
		return filler02;
	}
	public void setFiller02(String filler02) {
		this.filler02 = filler02;
	}
	public String getFiller03() {
		return filler03;
	}
	public void setFiller03(String filler03) {
		this.filler03 = filler03;
	}
	public String getFiller() {
		return filler;
	}
	public void setFiller(String filler) {
		this.filler = filler;
	}

	public String getRetCode() {
		return retCode;
	}
	public void setRetCode(String retCode) {
		this.retCode = retCode;
	}
	public String getRetMsg() {
		return retMsg;
	}
	public void setRetMsg(String retMsg) {
		this.retMsg = retMsg;
	}
	public String getCouponId() {
		return couponId;
	}
	public void setCouponId(String couponId) {
		this.couponId = couponId;
	}
	public String getPinId() {
		return pinId;
	}
	public void setPinId(String pinId) {
		this.pinId = pinId;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getBarcodeImgUrl() {
		return barcodeImgUrl;
	}
	public void setBarcodeImgUrl(String barcodeImgUrl) {
		this.barcodeImgUrl = barcodeImgUrl;
	}
	public String getResultMsg() {
		return resultMsg;
	}
	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}

	
}
