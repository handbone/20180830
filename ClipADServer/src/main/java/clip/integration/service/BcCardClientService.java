package clip.integration.service;

import java.util.concurrent.Future;

import clip.integration.bean.BcCardMessage;

public interface BcCardClientService {

	//포인트 조회
	public Future<BcCardMessage> getPoint(BcCardMessage param);
	
	//포인트 차감
	public Future<BcCardMessage> minusPoint(BcCardMessage param);
	
	//포인트 차감 취소
	public Future<BcCardMessage> cancelMinusPoint(BcCardMessage param);
	
	//일대사
	public int calculateBcPointSwap() throws Exception;
	public int calculateBcPointSwap(String preDate) throws Exception;
	public int processBcPointSwap(String preDate) throws Exception;

}
