package clip.integration.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.codehaus.plexus.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import com.coopnc.cardpoint.CardPointSwapManager;
import com.coopnc.cardpoint.consts.Consts.BcConsts;
import com.coopnc.cardpoint.domain.FtpFileInfo;
import com.coopnc.cardpoint.domain.PointSwapInfo;
import com.coopnc.cardpoint.handler.PointHandler_BC;
import com.coopnc.cardpoint.util.MessageUtil;

import clip.common.util.DateUtil;
import clip.integration.bean.BcCardMessage;
import clip.pointswap.dao.PointSwapDao;

@Service("bcCardClientService")
public class BcCardClientServiceImpl implements BcCardClientService {

	Logger logger = Logger.getLogger(this.getClass());
	
	@Value("#{props['POINT_SWAP_INI_FILE']}") private String pointSwapIniFile;
	
	@Value("#{props['API_DOMAIN']}") private String API_DOMAIN;
	
	private Boolean fakeUserCi;
	
	@Autowired
	private PointSwapDao pointSwapDao;
	
	@PostConstruct
	public void init() {
		if( API_DOMAIN !=null && API_DOMAIN.contains("127.0.0.1") ) {
			fakeUserCi =true;
		}else {
			fakeUserCi =false;
		}
		logger.info("bc Card fakecheck " + String.valueOf(fakeUserCi));
	}
	@Override
	@Async
	public Future<BcCardMessage> getPoint(BcCardMessage param) {
		try {
			if(fakeUserCi) {
				param.setUser_ci("zaau87y/eW5BWwlFhEtsVpptVEo5832gnqNVn0FQddh8bB5U45b9UZLOTzSB2lGkyNFKdXhFOgdTebs1qswA9Q==");
			}

			CardPointSwapManager manager = new CardPointSwapManager(getSwapConfigFile());
			
			int ret = pointSwapDao.selectDailySerialNo(BcConsts.system_name);
			String trId = manager.makeDefaultTrId(BcConsts.system_name, "", ""+ ret, "1");
			
			logger.debug(" generated trId : "+trId);

			param.setReg_source(BcConsts.system_name);
			param.setTrans_id(trId);
			param.setTrans_req_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
				
			manager.setHandler(new PointHandler_BC(param)).getPoint();
			
			param.setTrans_res_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
			
			if(!"S".equals(param.getResult())) {
				if(StringUtils.isEmpty(param.getRes_code())) {
					
					// 통신 오류나 연동 실패 시 03으로 리턴
					param.setResult("F");
					param.setRes_code("03");
				}
			}
			
			return new AsyncResult<BcCardMessage>(param);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	@Async
	public Future<BcCardMessage> minusPoint(BcCardMessage param) {
		try {
			if(fakeUserCi) {
				param.setUser_ci("zaau87y/eW5BWwlFhEtsVpptVEo5832gnqNVn0FQddh8bB5U45b9UZLOTzSB2lGkyNFKdXhFOgdTebs1qswA9Q==");
			}
			if(param.isPointOver() || param.getReq_point() == 0){
				param.setResult("F");
				return new AsyncResult<BcCardMessage>(param);
			}
			
			CardPointSwapManager manager = new CardPointSwapManager(getSwapConfigFile());

			int ret = pointSwapDao.selectDailySerialNo(BcConsts.system_name);
			String trId = manager.makeDefaultTrId(BcConsts.system_name, "", ""+ ret, "2");
			
			logger.debug(" generated trId : "+trId);

			param.setReg_source(BcConsts.system_name);
			param.setTrans_id(trId);
			param.setTrans_req_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
				
			manager.setHandler(new PointHandler_BC(param)).usePoint();
			param.setTrans_res_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
			pointSwapDao.insertPointSwapHist(param);

			// 비씨카드는 응답을 받아 접수일련번호를 받아야 취소가 가능하여 타임아웃 시 취소를 보낼 수 없음.
			if(!"S".equals(param.getResult())) {
//				//응답 코드가 없다면 내부 오류나 응답 지연으로 판단해 취소를 보낸다.
				if(StringUtils.isEmpty(param.getRes_code())) {
//					BcCardMessage cancelMsg = new BcCardMessage();
//					
//					ret = pointSwapDao.selectDailySerialNo(BcConsts.system_name);
//					trId = manager.makeDefaultTrId(BcConsts.system_name, "", ""+ ret, "3");
//
//					logger.debug(" generated trId : "+trId);
//
//					param.setReg_source(BcConsts.system_name);
//					cancelMsg.setUser_ci(param.getUser_ci());
//					cancelMsg.setCust_id_org(param.getCust_id_org());
//					cancelMsg.setRef_trans_id(param.getTrans_id());
//					cancelMsg.setTrans_id(trId);
//					cancelMsg.setReq_point(param.getReq_point());
//					
//					cancelMsg.setTrans_req_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
//					manager.setHandler(new PointHandler_BC(cancelMsg)).netCancelUse();
//					cancelMsg.setTrans_res_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
//					pointSwapDao.insertPointSwapHist(cancelMsg);
					
					// 통신 오류나 연동 실패 시 03으로 리턴
					param.setResult("F");
					param.setRes_code("03");
				}
			}

			return new AsyncResult<BcCardMessage>(param);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	@Async
	public Future<BcCardMessage> cancelMinusPoint(BcCardMessage param) {
		try {
			CardPointSwapManager manager = new CardPointSwapManager(getSwapConfigFile());
			
			if(fakeUserCi) {
				param.setUser_ci("zaau87y/eW5BWwlFhEtsVpptVEo5832gnqNVn0FQddh8bB5U45b9UZLOTzSB2lGkyNFKdXhFOgdTebs1qswA9Q==");
			}
			int ret = pointSwapDao.selectDailySerialNo(BcConsts.system_name);
			String trId = manager.makeDefaultTrId(BcConsts.system_name, "", ""+ ret, "3");

			logger.debug(" generated trId : "+trId);

			param.setReg_source(BcConsts.system_name);
			param.setRef_trans_id(param.getTrans_id());
			param.setTrans_id(trId);
			param.setTrans_req_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));

			manager.setHandler(new PointHandler_BC(param)).netCancelUse();
			param.setTrans_res_date(MessageUtil.getDateStr(new Date(), "yyyyMMddHHmmss"));
			
			pointSwapDao.insertPointSwapHist(param);
			
			return new AsyncResult<BcCardMessage>(param);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public int calculateBcPointSwap() throws Exception {
		Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat();
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        format.applyPattern("yyyyMMdd");
	    String preDate = format.format(calendar.getTime());
		return calculateBcPointSwap(preDate);
	}
	
	@Override
	public int calculateBcPointSwap(String preDate) throws Exception {
		try {
			PointSwapInfo param = new PointSwapInfo();
			
			param.setReg_source("bccard");
			param.setTrans_req_date(preDate);
			param.setResult("S");
			List<PointSwapInfo> pointSwapInfoList = pointSwapDao.selectSwapList(param);
			
			logger.debug("BC pointSwapInfoList : "+ pointSwapInfoList.size());
			CardPointSwapManager manager = new CardPointSwapManager(getSwapConfigFile());
			
			manager.setHandler(new PointHandler_BC(param));
			
			FtpFileInfo upFileInfo = new FtpFileInfo();
			upFileInfo.setStandDate(preDate);
			manager.makeLocalRecordFile(upFileInfo, pointSwapInfoList);
			
			return 1;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}
	
	@Override
	public int processBcPointSwap(String preDate) throws Exception {
		try {
			CardPointSwapManager manager = new CardPointSwapManager(getSwapConfigFile());
			manager.setHandler(new PointHandler_BC(null));
			FtpFileInfo downFileInfo = new FtpFileInfo();
			List<PointSwapInfo> remoteSwapInfoList = null;
			if (preDate == null || "".equals(preDate)) {
				remoteSwapInfoList = manager.parseRemoteRecordFile(downFileInfo);
			}else {
				SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
				Date date=format.parse(preDate);
				Calendar cal= Calendar.getInstance();
				cal.setTime(date);
				cal.add(Calendar.DATE, 1);
				String fileDate=format.format((cal.getTime()));
				
				remoteSwapInfoList = manager.parseRemoteRecordFileDate(downFileInfo,fileDate);
			}
			PointSwapInfo param = new PointSwapInfo();
			
			if (remoteSwapInfoList == null) {
				return 0;
			}
			
			Map<String, PointSwapInfo> map = new HashMap<String, PointSwapInfo>();
			for (PointSwapInfo info : remoteSwapInfoList) {
				map.put(info.getTrans_id(), info);
			}
			
			if (preDate == null || "".equals(preDate)) {
				param.setTrans_req_date(MessageUtil.getDateStr(DateUtil.addDate(-1), "yyyyMMdd"));
			} else {
				param.setTrans_req_date(preDate);
			}
			
			param.setReg_source("bccard");
			param.setResult("");
			List<PointSwapInfo> swapHistList = pointSwapDao.selectSwapList(param);
			int listCount = swapHistList.size();
	
			logger.debug("bc remote swapHistList size = " + remoteSwapInfoList.size());
			logger.debug("local swapHistList size = " + listCount);
			
			int cancelCount = 0;
			
			for (int i = 0; i<listCount; i++){
				PointSwapInfo pointInfo = swapHistList.get(i);
				
				if(map.get(pointInfo.getTrans_id()) != null && !"S".equals(pointInfo.getResult())){
					try {
						PointSwapInfo remoteInfo = map.get(pointInfo.getTrans_id());
						pointSwapDao.updateSwapInfoApproveNo(remoteInfo);
						
						//pre_idx 로 키를 잡고 바로 넣어버린다. 중복이면 안들어가도록
						pointInfo.setTrans_req_date(MessageUtil.getDateStr("yyyyMMdd"));
						pointSwapDao.insertPointSwapCancelInfo(pointInfo);
						cancelCount++;
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
					}
				}
			}
			
			logger.info("processBcPointSwap cancelCount = " + cancelCount);
			
			return 1;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			e.printStackTrace();
			throw e;
		}
	}

	private String getSwapConfigFile() {
		try {
			String result = new ClassPathResource(pointSwapIniFile).getURI().getPath();
			return result;
		} catch (IOException e) {
			return "";
		}
	}
}
