package clip.integration.service;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import clip.common.crypto.AES256CipherClip;
import clip.common.util.HttpClientUtil;
import clip.framework.BaseConstant;
import clip.framework.HttpNetwork;
import clip.integration.bean.ClipPointMessage;

@Service
public class ClipPointClientServiceImpl implements ClipPointClientService {
	Logger logger = Logger.getLogger(this.getClass());
	
	@Value("#{props['API_DOMAIN']}")
	private String API_DOMAIN;
	
	private static final String PLUS_POINT_URL = "adv/plusPoint.do";
	
	@Autowired
	private HttpNetwork httpNetwork;
	
	@Override
	public ClipPointMessage getPoint(ClipPointMessage param) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ClipPointMessage plusPoint(ClipPointMessage param) throws Exception {

		ClipPointMessage result = new ClipPointMessage();
		boolean userTokenYn = false;
		
		HttpClientUtil conn = null;
		try {
			conn = new HttpClientUtil("POST", API_DOMAIN + PLUS_POINT_URL);
			conn.addHeader("Content-Type", "application/x-www-form-urlencoded");
			
			if(StringUtils.isEmpty(param.getUser_ci())) {
				if(StringUtils.isEmpty(param.getUser_token())) {
					result.setResult("F");
					result.setResultMsg("포인트 적립 처리도중 오류가 발생하였습니다.(사용자 키가 전달되지 않았습니다.)");
					throw new Exception("사용자 키가 전달되지 않음");
				} else {
					conn.addParameter("user_token", URLEncoder.encode(param.getUser_token(),BaseConstant.DEFAULT_ENCODING));
					userTokenYn = true;
				}
			} else {
				conn.addParameter("user_ci", URLEncoder.encode(param.getUser_ci(),BaseConstant.DEFAULT_ENCODING));
				userTokenYn = true;
			}

			if(userTokenYn) {
				conn.addParameter("transaction_id", param.getTr_id());
				conn.addParameter("requester_code", param.getReg_source());
				conn.addParameter("point_value", ""+param.getPoint());
				conn.addParameter("description", URLEncoder.encode(param.getDescription(),BaseConstant.DEFAULT_ENCODING));
				
				int ret = conn.sendRequest();
				logger.debug("RES_HTTP_STATUS : " + ret);
	
				if(HttpStatus.SC_OK == ret) {
					result = conn.getResponseJson(ClipPointMessage.class);
					result.setResult("S");
					result.setResultMsg("정상 처리 되었습니다.");
					logger.debug("Plus Point API URL Response : code "+ret);
				} else {
					String resMsg = conn.getResponseString();
					
					if(resMsg != null && resMsg.contains("code: 600")) {
						result.setResult("D");
						result.setResultMsg("포인트 중복 적립입니다.");
						logger.debug("CLIPPOINTERROR 305:pointPlus http api error.");
					} else {
						result.setResult("F");
						result.setResultMsg("포인트 적립 처리도중 오류가 발생하였습니다.");
						logger.debug("CLIPPOINTERROR 300:pointPlus http api error.");
					}
				}
			}
			
		} catch (Exception e) {
			conn.closeConnection();
			logger.error(e.getMessage());
			result.setResult("F");
			result.setResultMsg("포인트 적립도중 Exception 오류가 발생하였습니다.");
			logger.debug("Plus Point API URL Response : "+e.getMessage());
		}
		
		return result;
	}

	@Override
	public ClipPointMessage minusPoint(ClipPointMessage param) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ClipPointMessage> getPointHistoryList(ClipPointMessage param) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ClipPointMessage cancelPlusPoint(ClipPointMessage param) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ClipPointMessage cancelMinusPoint(ClipPointMessage param) {
		// TODO Auto-generated method stub
		return null;
	}

	public int procClipPoint(ClipPointMessage param, ClipPointMessage.RegType regType) throws Exception {

		StringBuffer log = new StringBuffer();
    	log.append("\r\n===================================================================\r\n");
    	
    	log.append("Request Time=" + getCurrTime("yyyy.MM.dd HH:mm:ss.SSS") + "\r\n");
    	
    	String description = "["+param.getReg_source()+"] 포인트 전환";

    	//사용자 키 결정
    	String idString = makeIdString(param, regType);
    	
		String endUrl = idString
						+"&transaction_id="+param.getTr_id()
						+"&requester_code="+BaseConstant.REQUESTER_CODE
						+"&point_value="+param.getPoint()
						+"&description="+java.net.URLEncoder.encode(description,BaseConstant.DEFAULT_ENCODING);
		endUrl = endUrl.replaceAll("(\r\n|\n)", "");
		endUrl = endUrl.replaceAll(System.getProperty("line.separator"), "");
		
		// Plus Point API 접속 URL
		logger.debug("Plus Point API URL : "+BaseConstant.ADV_PLUS_POINT+"?"+endUrl);
		
		String strHtmlSource = "";

		String errMsg = "";
		if(param.getPoint() == 0){
			strHtmlSource = "No Request.";
		} else {
			try { 
				// 실제 통신이 일어나는 부분
				strHtmlSource = httpNetwork.strGetData(BaseConstant.ADV_PLUS_POINT, endUrl);
				logger.debug("Plus Point API URL Response : "+strHtmlSource);
			} catch (Exception e ){
				errMsg = e.getMessage();
				// 통신에서 에러가 발생했을때 에러 메시지를 로그로 남긴다
				logger.debug("Plus Point API URL Response : "+e.getMessage());
				if(errMsg.contains("code: 600")){
					logger.debug("CLIPPOINTERROR 305:pointPlus http api error.");
	        	} else {
	        		logger.debug("CLIPPOINTERROR 300:pointPlus http api error.");
	        	}
			}
		}
		
    	HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.currentRequestAttributes()).getRequest();

    	log.append("Request URL:" + request.getRequestURL().toString() + "\r\n");
    	log.append("Plus Point API URL :" + BaseConstant.ADV_PLUS_POINT+ "\r\n");
    	log.append("Plus Point API Parameter [idString]:" + idString + "\r\n");
    	log.append("Plus Point API Parameter [transaction_id]:" + param.getTr_id() + "\r\n");
    	log.append("Plus Point API Parameter [requester_code]:" + BaseConstant.REQUESTER_CODE + "\r\n");
    	log.append("Plus Point API Parameter [point_value]:" + param.getPoint()+ "\r\n");
    	log.append("Plus Point API Parameter [description]:" + description + "\r\n");
    	if ("".equals(strHtmlSource)){ // 통신실패 혹은 중복적립(600)
    		log.append("Plus Point API Response :" + errMsg + "\r\n");
    	} else {
    		log.append("Plus Point API Response :" + strHtmlSource + "\r\n");
    	}
    	
    	//log.append("Request IP=" + request.getRemoteAddr() + "\r\n");
    	log.append("Response Time=" + getCurrTime("yyyy.MM.dd HH:mm:ss.SSS") + "\r\n");
    	log.append("===================================================================\r\n");
        logger.info(log.toString());

        if("".equals(strHtmlSource)){ // 통신실패 혹은 중복적립
        	if(errMsg.contains("code: 600")){ // 중복적립
        		param.setResult("3");
        		return 3;
        	}else{
        		param.setResult("0");
        		return 0;
        	}
		} else { // 성공
			param.setResult("1");
			return 1;
		}
	}
	
	private static String getCurrTime(String outFormat){
		Calendar calendar = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat();
        format.applyPattern(outFormat);
        return format.format(calendar.getTime());
	}
	
	private static String makeIdString(ClipPointMessage param, ClipPointMessage.RegType regType) throws Exception {

		if(regType == ClipPointMessage.RegType.GET_POINT_HISTORY
				|| regType == ClipPointMessage.RegType.GET_POINT
				|| regType == ClipPointMessage.RegType.PLUS_POINT
				|| regType == ClipPointMessage.RegType.PLUS_POINT_CANCEL
				|| regType == ClipPointMessage.RegType.MINUS_POINT
				|| regType == ClipPointMessage.RegType.MINUS_POINT_CANCEL ) {
			
			if(StringUtils.isEmpty(param.getUser_ci())) {
				if(StringUtils.isEmpty(param.getUser_token())) {
					throw new Exception("user_ci or user_token requeired !!");
				} else {
					return "user_token="+java.net.URLEncoder.encode(param.getUser_token(),BaseConstant.DEFAULT_ENCODING);
				}
			} else {
				return "user_ci="+java.net.URLEncoder.encode(param.getUser_ci(),BaseConstant.DEFAULT_ENCODING);
			}
		} else {
			
			if(!StringUtils.isEmpty(param.getUser_ci()))
				return "user_ci="+java.net.URLEncoder.encode(param.getUser_ci(),BaseConstant.DEFAULT_ENCODING);
			
			if(!StringUtils.isEmpty(param.getUser_token()))
				return "user_token="+java.net.URLEncoder.encode(param.getUser_token(),BaseConstant.DEFAULT_ENCODING);
			
			if(!StringUtils.isEmpty(param.getCust_id()))
				return "cust_id=" + java.net.URLEncoder.encode(param.getCust_id(),BaseConstant.DEFAULT_ENCODING);

			if(!StringUtils.isEmpty(param.getCtn()))
				return "ctn=" + java.net.URLEncoder.encode(new String(AES256CipherClip.AES_Encode(param.getCtn()).getBytes()), BaseConstant.DEFAULT_ENCODING);

			if(!StringUtils.isEmpty(param.getGa_id()))
				return "ga_id=" + java.net.URLEncoder.encode(new String(AES256CipherClip.AES_Encode(param.getGa_id()).getBytes()),BaseConstant.DEFAULT_ENCODING);
			
			throw new Exception("one of user_ci,user_token,cust_id,ctn,ga_id requeired !!");
		}
		
	}
	
}
