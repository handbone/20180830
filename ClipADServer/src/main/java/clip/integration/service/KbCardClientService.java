package clip.integration.service;

import java.util.concurrent.Future;

import clip.integration.bean.KbCardMessage;

public interface KbCardClientService {

	//포인트 조회
	public Future<KbCardMessage> getPoint(KbCardMessage param);
	
	//포인트 차감
	public Future<KbCardMessage> minusPoint(KbCardMessage param);
	
	//포인트 차감 취소
	public Future<KbCardMessage> cancelMinusPoint(KbCardMessage param);
	
	//일대사
	public Future<KbCardMessage> uploadBalanceAccounts(KbCardMessage param);

}
