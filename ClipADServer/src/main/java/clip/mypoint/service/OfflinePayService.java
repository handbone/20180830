package clip.mypoint.service;

import java.util.List;

import clip.mypoint.bean.OfflinePayBean;

public interface OfflinePayService {
	
	/**
	 * 오프라인 결제 아이템 리스트 조회
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public List<OfflinePayBean> getItemList(OfflinePayBean param) throws Exception;
	
	/**
	 * 오프라인 결제 아이템 정보 상세
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public OfflinePayBean getItemInfo(OfflinePayBean param) throws Exception;
	
	/**
	 * 오프라인 결제 핀번호 조회
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public OfflinePayBean getPinId(OfflinePayBean param) throws Exception;
	
	/**
	 * 오프라인 결제 아이템 바코드 페이지 상세
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public OfflinePayBean getBarcodeInfo(OfflinePayBean param) throws Exception;
	
}
