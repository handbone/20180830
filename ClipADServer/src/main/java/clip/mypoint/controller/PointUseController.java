package clip.mypoint.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import clip.mypoint.bean.OfflinePayBean;
import clip.mypoint.service.OfflinePayService;

@Controller
@RequestMapping("/pointuse")
public class PointUseController {
	Logger log = Logger.getLogger(this.getClass());
	
	@Value("#{props['COOP_COUPON_CODE_EDIYA']}")
	private String COOP_COUPON_CODE_EDIYA;
	
	@Resource(name="offlinePayService")
	private OfflinePayService offlinePayService;

	/** 포인트 쓰기 텝 화면 **/
	@RequestMapping(value="/main.do")
	public ModelAndView mypoint ( HttpServletRequest request, Model model) throws java.lang.Exception {
		
		ModelAndView mv = new ModelAndView("/clip/pointuse/main");	
		
		String cust_id = (String) request.getParameter("cust_id");
		String ctn = (String) request.getParameter("ctn");
		String gaid = (String) request.getParameter("gaid");
		String offerwall = (String) request.getParameter("offerwall");
		
		String user_ci = (String) request.getParameter("user_ci");
		
		//user token 추가
		String user_token = (String) request.getParameter("user_token");
		
		transferCommonParameter(request, mv);
		
		return mv;
	}
	
	/** 오프라인 간편 결제 메인 화면 **/
	@RequestMapping(value="/offlinePayMain.do")
	public ModelAndView offlinePayMain(@ModelAttribute OfflinePayBean param, HttpServletRequest request, Model model) throws java.lang.Exception {
		
		//TODO 오프라인이 이디야 밖에 없어서 디테일로 바로 보내는 것으로 임시 조치
		//ModelAndView mv = new ModelAndView("/clip/pointuse/offlinePayMain");	<- 아래거 지우고 주석 지움
		//-------- 임시 : s ----------
		ModelAndView mv = new ModelAndView("/clip/pointuse/offlinePayDetail");
		String item_id = COOP_COUPON_CODE_EDIYA;	//이디야
		
		mv.addObject("item_id", item_id);
		param.setItem_id(item_id);
		OfflinePayBean result = offlinePayService.getItemInfo(param);
		mv.addObject("itemInfo", result);
		//-------- 임시 : e ----------
		
		transferCommonParameter(request, mv);

		mv.addObject("itemList", offlinePayService.getItemList(param));
		
		return mv;
	}
	
	/** 오프라인 간편 결제 상품별 상세 **/
	@RequestMapping(value="/offlinePayDetail.do")
	public ModelAndView offlinePayDetail(@ModelAttribute OfflinePayBean param, HttpServletRequest request, Model model) throws java.lang.Exception {
		
		ModelAndView mv = new ModelAndView("/clip/pointuse/offlinePayDetail");	
		
		String cust_id = (String) request.getParameter("cust_id");
		String ctn = (String) request.getParameter("ctn");
		String gaid = (String) request.getParameter("gaid");
		String offerwall = (String) request.getParameter("offerwall");
		
		String user_ci = (String) request.getParameter("user_ci");	
		//user token 추가
		String user_token = (String) request.getParameter("user_token");
		
		transferCommonParameter(request, mv);
		
		OfflinePayBean result = offlinePayService.getItemInfo(param);
		mv.addObject("itemInfo", result);
		
		return mv;
	}
	
	/** 오프라인 간편 결제 핀번호 조회 **/
	@RequestMapping(value = "/offlinePayPinId.do", method = { RequestMethod.POST }, produces = "application/json; charset=utf-8")
	@ResponseBody
	public ModelAndView offlinePayPinId(@RequestBody OfflinePayBean param, HttpServletRequest request, Model model) throws java.lang.Exception {
	
		ModelAndView mv = new ModelAndView("jsonView");
		
		String cust_id = (String) request.getParameter("cust_id");
		String ctn = (String) request.getParameter("ctn");
		String gaid = (String) request.getParameter("gaid");
		String offerwall = (String) request.getParameter("offerwall");
		
		String user_ci = (String) request.getParameter("user_ci");	
		//user token 추가
		String user_token = (String) request.getParameter("user_token");
		
		transferCommonParameter(request, mv);
		
		OfflinePayBean result = offlinePayService.getPinId(param);
		mv.addObject("itemInfo", result);
		
		return mv;
	}
	
	/** 오프라인 간편 결제 바코드 표시 **/
	@RequestMapping(value="/offlinePayBacode.do")
	public ModelAndView offlinePayBarcode(@ModelAttribute OfflinePayBean param, HttpServletRequest request, Model model) throws java.lang.Exception {
		
		ModelAndView mv = new ModelAndView("/clip/pointuse/offlinePayBacode");
		
		String cust_id = (String) request.getParameter("cust_id");
		String ctn = (String) request.getParameter("ctn");
		String gaid = (String) request.getParameter("gaid");
		String offerwall = (String) request.getParameter("offerwall");
		
		String user_ci = (String) request.getParameter("user_ci");	
		//user token 추가
		String user_token = (String) request.getParameter("user_token");
		
		transferCommonParameter(request, mv);
		
		OfflinePayBean result = offlinePayService.getBarcodeInfo(param);
		mv.addObject("itemInfo", result);
		
		return mv;
	}
	
	/**
	 * 파라미터로 전달된 공통 인자 값을 다음 페이지 모델에 포함 시킨다.
	 * @param request
	 * @param mv
	 */
	private void transferCommonParameter(HttpServletRequest request, ModelAndView mv) {
		String cust_id = (String) request.getParameter("cust_id");
		String ctn = (String) request.getParameter("ctn");
		String gaid = (String) request.getParameter("gaid");
		String offerwall = (String) request.getParameter("offerwall");
		
		String user_ci = (String) request.getParameter("user_ci");
		
		//user token 추가
		String user_token = (String) request.getParameter("user_token");
		
		if(cust_id != null && gaid != null){
			mv.addObject("cust_id", cust_id.replaceAll("[\r\n]", ""));
			if(null != ctn && !"".equals(ctn)){
				mv.addObject("ctn", ctn.replaceAll("[\r\n]", ""));	
			}
			mv.addObject("gaid", gaid.replaceAll("[\r\n]", ""));
			
			if(user_ci != null){
				mv.addObject("user_ci", user_ci.replaceAll("[\r\n]", ""));	
			}
			
			if(offerwall != null){
				mv.addObject("offerwall", offerwall.replaceAll("[\r\n]", ""));
			}
			
			if(user_token != null){
				mv.addObject("user_token", user_token.replaceAll("[\r\n]", ""));
			}
		}
	}
	
}


