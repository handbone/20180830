package clip.mypoint.bean;

import java.util.List;

/**
 * CLIP3.0 메인베너
 * 2017.11.03
 * @author jyi
 */
public class MainbannerInfoBean {
	
	private String description;
	private String end_date;   
	private String link_url;
	private String mb_id;
	private String mb_name;
	private String mbimg_url;
	private String os_type;
	private String rank;
	private String reg_date;
	private String service_status;
	private String start_date;
	private String url_type;
	
	public String getUrl_type() {
		return url_type;
	}
	public void setUrl_type(String url_type) {
		this.url_type = url_type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getEnd_date() {
		return end_date;
	}
	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}
	public String getLink_url() {
		return link_url;
	}
	public void setLink_url(String link_url) {
		this.link_url = link_url;
	}
	public String getMb_id() {
		return mb_id;
	}
	public void setMb_id(String mb_id) {
		this.mb_id = mb_id;
	}
	public String getMb_name() {
		return mb_name;
	}
	public void setMb_name(String mb_name) {
		this.mb_name = mb_name;
	}
	public String getMbimg_url() {
		return mbimg_url;
	}
	public void setMbimg_url(String mbimg_url) {
		this.mbimg_url = mbimg_url;
	}
	public String getOs_type() {
		return os_type;
	}
	public void setOs_type(String os_type) {
		this.os_type = os_type;
	}
	public String getRank() {
		return rank;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}
	public String getReg_date() {
		return reg_date;
	}
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
	public String getService_status() {
		return service_status;
	}
	public void setService_status(String service_status) {
		this.service_status = service_status;
	}
	public String getStart_date() {
		return start_date;
	}
	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}
}
