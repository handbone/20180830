/*[CLiP Point] version [v1.0]
* Copyright © 2016 kt corp. All rights reserved.
* This is a proprietary software of kt corp, and you may not use this file except in
* compliance with license agreement with kt corp. Any redistribution or use of this
* software, with or without modification shall be strictly prohibited without prior written
* approval of kt corp, and the copyright notice above does not evidence any actual or
* intended publication of such software.
*/
package clip.framework;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonValueProcessor;


public class JavaUtilDateJsonValueProcessor implements JsonValueProcessor {
	/** 데이트 포맷 정의 */
	public DateFormat	defaultDateFormat	= null;

	/**
	 * 생성자 (Java Date 타입을 yyyy.MM.dd HH:mm:ss 형태로 변환한다.)
	 */
	public JavaUtilDateJsonValueProcessor() {
		this.defaultDateFormat	= new SimpleDateFormat("yyyy.MM.dd HH:mm:ss", Locale.KOREA);
	}

    @Override
	public Object processArrayValue(Object value, JsonConfig jsonConfig) {
		if (null == value) {
			return null;
		}

		return this.defaultDateFormat.format(value);
	}

	@Override
	public Object processObjectValue(String key, Object value, JsonConfig jsonConfig) {
		return processArrayValue(value, jsonConfig);
	}
}
