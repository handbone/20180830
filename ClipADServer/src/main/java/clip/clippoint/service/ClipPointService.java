package clip.clippoint.service;

import javax.servlet.http.HttpServletRequest;

import clip.roulette.bean.PointSwap;

public interface ClipPointService {
	
	public int procClipPoint(HttpServletRequest request, PointSwap param, int targetSystem, int inout, String tr_id, int point) throws Exception;
	
	/**
	 * 포인트 적립
	 * @param requester_code
	 * @param user_ci
	 * @param tr_id
	 * @param point
	 * @param description
	 * @return
	 * @throws Exception
	 */
	public int plusPoint(String requester_code, String user_ci, String tr_id, int point, String description) throws Exception;
}
