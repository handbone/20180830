<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<title>CLIP</title>
	
	<!-- 이전 CLIP JS 및 CSS 파일
	<script type="text/javascript" src="../js/common/jquery-1.8.3.js"></script>
	<script type="text/javascript" src="../js/common/common.js"></script>
	<script type="text/javascript" src="../js/common/jquery.json-2.4.js"></script>
	<script type="text/javascript" src="../js/my_point/myPoint.js"></script>
	<link rel="stylesheet" type="text/css" href="../css/mypoint.css"/>
	bx slider lib
	<script type="text/javascript" src="../js/my_point/jquery.bxslider.js"></script> 
	-->
	
	<!-- to-be  -->
	<link rel="stylesheet" href="../css/lib/swiper.min.css">
	<link rel="stylesheet" href="../css/common.css?20170905">
</head>
<body>
<!-- wrap -->
<div class="wrap noheader">
	<!-- header -->
	<div class="header">
		<h1 class="page_title">랜덤 포인트 폭탄</h1>
		<a href="#" class="btn_back" onclick="javascript:history.back()"><span>뒤로가기</span></a>
	</div><!-- // header -->
	<!-- contents -->
	<div class="contents">
		<div class="bomb_wrap">
			<div class="bomb_top fixed_top">
				<img src="../images/bomb/bomb_top.png" class="img_top_txt" alt="3일마다 팡팡 터지는 랜덤 포인트 폭탄!">
			</div>
			<div class="bomb_touch">
				<a href="#">
					<!--  말풍선  -->
					<span class="bomb_sb">
						<img src="../images/bomb/bomb_touch1.png" alt="폭탄을 터치하여 터드리세요!">
					</span>
					<!--  // 말풍선  -->
					<span class="btn_touch">
						<img src="../images/bomb/bomb_touch2.png" alt="TOUCH!!">
					</span>
					<!--  폭탄  -->
					<span class="bomb">
						<img src="../images/bomb/bomb_touch3.png" alt="폭탄">
					</span>
					<!--  //폭탄  -->
					<!-- 폭탄 심지 -->
					<span class="bomb_wick">
						<img src="../images/bomb/bomb_touch6.png" alt="">
					</span>
					<span class="bomb_wick2">
						<img src="../images/bomb/bomb_touch5.png" alt="">
					</span>
					<!-- // 폭탄 심지 -->
					<!-- 폭탄 그림자 -->
					<span class="bomb_shadow">
						<img src="../images/bomb/bomb_touch_shadow.png" alt="">
					</span>
					<!-- // 폭탄 그림자 -->
				</a>
				<div class="lv_txt">회원님의 폭탄레벨 : lv.30</div>
			</div>
			<div class="bomb_tip">
				<img src="../images/bomb/bomb_tip.jpg" alt="">
				<dl class="blind">
					<dt>폭탄레벨 올리는 꿀팁!</dt>
					<dd>잠금 화면을 풀 때, 왼쪽으로 슬라이드를 많이 할 수록 레벨 급상승!</dd>
					<dd>레벨이 오르면 최소 랜덤포인트도 함께 업!</dd>
				</dl>
			</div>
		</div>
	</div><!-- // contents -->
</div><!-- // wrap -->
<script src="../js/lib/jquery-1.12.4.min.js"></script>
<script src="../js/lib/swiper.min.js"></script>
<script src="../js/ui.js?20170905"></script>
<script>


 //    var boomMove ={
	//     init:function(){
	//         this.btn = $('.btn_touch');
	//         this.fire = $('.bomb_wick2');
	//         this.addEvent();
	//     },
	//     addEvent:function(){
	//         var _this = this;
	//         setInterval(function(){
	//         	if(!_this.btn.hasClass('active')){
	//         		_this.btn.addClass('active');
	//         	}else{
	//         		_this.btn.removeClass('active');
	//         	}	    		
	//         },500);

	//         setInterval(function(){
	//         	if(!_this.btn.hasClass('active')){
	//         		_this.fire.addClass('active');
	//         	}else{
	//         		_this.fire.removeClass('active');
	//         	}	    		
	//         },500);
	//     },
	//     txtMove:function(){
	    	
	    	
	    	
	//     }
	// }
	// boomMove.init(); 
</script>
</body>
</html>