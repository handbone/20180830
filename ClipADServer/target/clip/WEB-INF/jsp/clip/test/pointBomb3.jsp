<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="pragma" content="no-cache" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<title>CLIP</title>
	
	<!-- 이전 CLIP JS 및 CSS 파일
	<script type="text/javascript" src="../js/common/jquery-1.8.3.js"></script>
	<script type="text/javascript" src="../js/common/common.js"></script>
	<script type="text/javascript" src="../js/common/jquery.json-2.4.js"></script>
	<script type="text/javascript" src="../js/my_point/myPoint.js"></script>
	<link rel="stylesheet" type="text/css" href="../css/mypoint.css"/>
	bx slider lib
	<script type="text/javascript" src="../js/my_point/jquery.bxslider.js"></script> 
	-->
	
	<!-- to-be  -->
	<link rel="stylesheet" href="../css/lib/swiper.min.css">
	<link rel="stylesheet" href="../css/common.css?20170905">
</head>
<body>
<!-- wrap -->
<div class="wrap noheader">
	<!-- header -->
	<div class="header">
		<h1 class="page_title">랜덤 포인트 폭탄</h1>
		<a href="#" class="btn_back" onclick="javascript:history.back()"><span>뒤로가기</span></a>
	</div><!-- // header -->
	<!-- contents -->
	<div class="contents contents_h">
		<div class="bomb_wrap nobg">
			<div class="bomb_top">
				<img src="../images/temp/bomb_top_ban.jpg" class="img_bomb_ban" alt="">
				<div class="blind">
					<span>3일에 한번 터지는 폭탄! 기다리기 힘드시다면?</span>
					<p>이벤트 참여하면 즉시 폭탄이! 추가 포인트는 더더더!!! </p>
					<p>이미 참여하신 이벤트는 대상에서 제외됩니다.</p>
				</div>
			</div>
			<ul class="bomb_list">
				<li>
					<a href="">
						<i><img src="../images/temp/bomb_cp.jpg" alt="현대아울렛"></i>
						<strong class="top_title">현대아울렛 페이스북 좋아요</strong>
						<p>캠페인에 참여하세요.</p>
						<span>참여형</span>
					</a>
				</li>
				<li>
					<a href="">
						<i><img src="../images/temp/bomb_cp2.jpg" alt="다음"></i>
						<strong>다음 - Daum</strong>
						<p>앱 설치 후 앱을 실행하세요.</p>
						<span>실행형</span>
					</a>
				</li>
				<li>
					<a href="">
						<i><img src="../images/temp/bomb_cp3.jpg" alt="하이마트"></i>
						<strong>하이마트 페이스북 좋아요</strong>
						<p>캠페인에 참여하세요.</p>
						<span>참여형</span>
					</a>
				</li>
				<li>
					<a href="">
						<i><img src="../images/temp/bomb_cp3.jpg" alt="하이마트"></i>
						<strong>하이마트</strong>
						<p>하이마트</p>
						<span>참여형</span>
					</a>
				</li>
			</ul>
			<div class="btn_wrap btn_fixed">
				<a href="#" class="btn_bomb_popout">폭탄 터트리기</a>
			</div>
		</div>
	</div><!-- // contents -->
</div><!-- // wrap -->
<script src="../js/lib/jquery-1.12.4.min.js"></script>
<script src="../js/lib/swiper.min.js"></script>
<script src="../js/ui.js?20170905"></script>
</body>
</html>