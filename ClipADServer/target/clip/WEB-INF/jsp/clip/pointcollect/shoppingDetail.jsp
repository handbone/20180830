<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<div class="deem"></div>
<div class="layer_pop">
	<div class="lp_hd">
		<!-- 170726 수정 -->
		<strong class="title"><img src="../images/temp/${itemInfo.item_image_url}" alt=""></strong>
		<!-- // 170726 수정 -->
		<span>${itemInfo.reward}% 적립</span>
	</div>
	<div class="lp_ct">
		<!-- point_tf_cont -->
		<div class="point_tf_cont">
			<div class="inr ex_txt">
				KT CLIP을 통해 쇼핑몰을 방문해서 상품을
				구매하면 추가로 포인트를 적립해 드립니다.<br><br>

				<c:if test="${itemInfo.description ne 'description'}">
					<strong>[적립 제외 상품 및 유의사항]</strong>
					<c:out value="${itemInfo.description}" escapeXml="false"/>
				</c:if>
			</div>
		</div>
		<!-- // point_tf_cont -->
		<div class="btn_multi_box">
			<button type="button" class="btn_lp_cancel" onclick="javascript:fn.layer.close('layer_pop_wrap');">취소</button>
			<button type="submit" class="btn_lp_confirm" onclick="javascript:fn.layer.submit()">쇼핑하러가기</button>
		</div>
	</div>
</div>
<form name="item_info">
	<input type="hidden" id="mall_id" name="mall_id" value="${ itemInfo.mall_id }" />
	<input type="hidden" id="item_id" name="item_id" value="${ itemInfo.item_id }" />
</form>