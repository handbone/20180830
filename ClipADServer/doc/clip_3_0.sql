--///////////////////////////////////////////////////////////////////////////////////////////////

CREATE TABLE tb_point_coupon_fail
(
	user_ci character varying(256) NOT NULL,
	last_try_date timestamp without time zone NOT NULL,
	error_cnt numeric DEFAULT 0,
	user_block character varying(1) NOT NULL,
	total_error_cnt numeric DEFAULT 0,
  	CONSTRAINT "tb_point_coupon_fail_pkey" PRIMARY KEY (user_ci)
)
WITH (
  OIDS=FALSE
);

ALTER TABLE tb_point_coupon_fail
  OWNER TO point;


CREATE INDEX idx_tb_point_coupon_fail_01
  ON tb_point_coupon_fail
  USING btree
  (user_ci);
  
  
  
--///////////////////////////////////////////////////////////////////////////////////////////////
